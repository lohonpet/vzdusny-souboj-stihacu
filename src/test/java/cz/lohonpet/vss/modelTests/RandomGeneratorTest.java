package cz.lohonpet.vss.modelTests;

import cz.lohonpet.vss.backend.components.Coordinates;
import cz.lohonpet.vss.backend.utils.RandomGenerator;
import org.junit.jupiter.api.Test;

import java.util.List;

import static cz.lohonpet.vss.utils.VSSConstants.JETS_START_COLUMNS_RANGE;
import static cz.lohonpet.vss.utils.VSSConstants.BOARD_WIDTH;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

class RandomGeneratorTest {
    private final Integer d6Low = 1;
    private final Integer d6High = 6;
    private final Integer loopCount = 1000;

    @Test
    void rollDieD6() {
        for (int i = 0; i < loopCount; i++) {
            Integer roll = RandomGenerator.rollDieD6();
            assertTrue(d6Low <= roll && roll <= d6High);
        }
    }

    @Test
    void rollNTimesDiceD6() {
        List<Integer> rolls = RandomGenerator.rollNTimesDiceD6(loopCount);
        for (Integer roll : rolls) {
            assertTrue(d6Low <= roll && roll <= d6High);
        }
    }

    @Test
    void tossCoin() {
        boolean canBeTrue = false, canBeFalse = false;
        while (true) {
            if (RandomGenerator.tossCoin()) {
                canBeTrue = true;
            } else {
                canBeFalse = true;
            }
            if (canBeTrue && canBeFalse) {
                break;
            }
        }
    }

    @Test
    void randomStartJetCoordinateforLeft() {
        for (int i = 0; i < loopCount; i++) {
            Coordinates testCoords = RandomGenerator.randomStartJetCoordinate(0);
            assertTrue(1 <= testCoords.getX() && testCoords.getX() <= 5);
            assertTrue(6 <= testCoords.getY() && testCoords.getY() <= 13);
        }
    }

    @Test
    void randomStartJetCoordinateForRight() {

        for (int i = 0; i < loopCount; i++) {
            Coordinates testCoords = RandomGenerator.randomStartJetCoordinate(BOARD_WIDTH - JETS_START_COLUMNS_RANGE);
            assertTrue(13 <= testCoords.getX() && testCoords.getX() <= 17);
            assertTrue(6 <= testCoords.getY() && testCoords.getY() <= 13);
        }
    }

    @Test()
    void randomStartJetCoordinateWithWrongParameter() {
        for (int i = 0; i < loopCount; i++) {
            assertThrows(IllegalArgumentException.class, () ->
                    RandomGenerator.randomStartJetCoordinate(15));
        }
    }
}