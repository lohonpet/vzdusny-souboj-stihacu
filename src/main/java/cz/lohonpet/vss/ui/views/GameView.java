package cz.lohonpet.vss.ui.views;

import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.router.*;
import cz.lohonpet.vss.backend.components.*;
import cz.lohonpet.vss.backend.presenters.GamePresenterInterface;
import cz.lohonpet.vss.ui.components.*;
import cz.lohonpet.vss.ui.utils.SoundLibrary;
import cz.lohonpet.vss.utils.VSSConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

import static cz.lohonpet.vss.utils.VSSConstants.*;

@Route(value = VSSConstants.URL_GAME, layout = MainView.class)
@PageTitle(value = TITLE_GAME)
@CssImport("./styles/gameStyles.css")
@Component
@Scope(PROTOTYPE)
public class GameView extends Composite<HorizontalLayout>
        implements GameViewInterface, HasComponents, HasUrlParameter<String> {
    private final GameBoard gameBoard;
    private final PlayerPanel leftPlayerPanel;
    private PlayerPanel rightPlayerPanel;
    private final GamePresenterInterface gamePresenter;
    private final InfoPopup infoPopup;
    private final InfoPopup settingsPopup;
    private final InfoPopup rulesPopup;
    private final SoundLibrary soundLibrary;
    private final RotateAnimationPopup tossCoinPopup;

    @Autowired
    public GameView(GamePresenterInterface gamePresenter, GameBoard gameBoard,
                    PlayerPanel leftPlayerPanel, PlayerPanel rightPlayerPanel,
                    SoundLibrary soundLibrary, InfoPopup infoPopup,
                    InfoPopup settingsPopup, InfoPopup rulesPopup,
                    RotateAnimationPopup tossCoinPopup) {
        this.leftPlayerPanel = leftPlayerPanel;
        this.gamePresenter = gamePresenter;
        this.gameBoard = gameBoard;
        this.infoPopup = infoPopup;
        this.rulesPopup = rulesPopup;
        this.settingsPopup = settingsPopup;
        this.soundLibrary = soundLibrary;
        this.tossCoinPopup = tossCoinPopup;
        this.rightPlayerPanel = rightPlayerPanel;
        this.leftPlayerPanel.setPlayerPanel(VELITEL_ONE, "left");
        this.rightPlayerPanel.setPlayerPanel(VELITEL_TWO, "right");
        this.infoPopup.setCloseOnEsc(false);
        this.infoPopup.setCloseOnOutsideClick(false);
        this.rulesPopup.setContentHtml(RULES);
        this.rulesPopup.getConfirmButton().addClickListener(ev -> rulesPopup.close());
        this.rulesPopup.setConfirmButtonText(BACK_BUTTON_TEXT);
        setSettingsPopup();
        this.tossCoinPopup.setCloseOnEsc(false);
        this.tossCoinPopup.setCloseOnOutsideClick(false);
        this.soundLibrary.init();
        add(this.leftPlayerPanel, this.gameBoard, this.rightPlayerPanel, this.infoPopup, this.soundLibrary);
    }

    @Override
    public void setParameter(BeforeEvent beforeEvent, String parameter) {
        if (parameter.equals(URL_PC)) {
            remove(rightPlayerPanel);
            gamePresenter.setGame(new VssGame(new Player(), new PlayerPc()));
            rightPlayerPanel = new PlayerPanelPc(gamePresenter);
            rightPlayerPanel.setPlayerPanel(PC_NAME, "right");
            add(rightPlayerPanel);
        } else {
            gamePresenter.setGame(new VssGame(new Player(), new Player()));
        }
        rightPlayerPanel.deactivate();
        gamePresenter.setGameView(this);
        gamePresenter.cleanEndGame();
        gameBoard.initBoard();
    }

    private void setSettingsPopup() {
        settingsPopup.setHeaderText(SETTINGS_BUTTON_TEXT);
        settingsPopup.setButtonToMainMenu();
        Button muteButton = new Button(MUTE_BUTTON_TEXT);
        Button unmuteButton = new Button(UNMUTE_BUTTON_TEXT);
        muteButton.addClickListener(e -> {
            gamePresenter.muteSounds();
            muteButton.addClassName("displayNone");
            unmuteButton.removeClassName("displayNone");
        });
        unmuteButton.addClickListener(e -> {
            gamePresenter.unmuteSounds();
            unmuteButton.addClassName("displayNone");
            muteButton.removeClassName("displayNone");
        });
        if (soundLibrary.isMuted()) {
            muteButton.addClassName("displayNone");
        } else {
            unmuteButton.addClassName("displayNone");
        }
        Button rulesButton = new Button(MENU_RULES);
        rulesButton.addClickListener(e -> {
            settingsPopup.close();
            rulesPopup.open();
        });
        Button backButton = new Button(BACK_BUTTON_TEXT);
        backButton.addClickListener(e -> settingsPopup.close());
        settingsPopup.addButton(muteButton);
        settingsPopup.addButton(unmuteButton);
        settingsPopup.addButton(rulesButton);
        settingsPopup.addButton(backButton);
    }


    @Override
    public void setJetsAfterClickDie(List<Coordinates> jetCoords, Integer roll) {
        gameBoard.setJetsAfterClickDie(jetCoords, roll);
    }

    @Override
    public boolean areAllDiceUsed(PlayerName player) {
        return choosePlayerPanel(player).areAllDiceUsed();
    }

    private PlayerPanel choosePlayerPanel(PlayerName player) {
        return player == PlayerName.PLAYER_LEFT
                ? leftPlayerPanel : rightPlayerPanel;
    }

    @Override
    public void deactivatePlayerUI(PlayerName player, List<Coordinates> jetCoordsByPlayer) {
        choosePlayerPanel(player).deactivate();
        gameBoard.deactiveJetsByPlayer(player, jetCoordsByPlayer);
    }

    @Override
    public void activatePlayerUI(PlayerName player, List<Coordinates> jetCoordsByPlayer) {
        choosePlayerPanel(player).activate();
        gameBoard.activeJetsByPlayer(player, jetCoordsByPlayer);

    }

    @Override
    public List<DieUI> getActiveDice() {
        return choosePlayerPanel(gamePresenter.getPlayerOnTurnName()).getActiveDice();
    }

    @Override
    public void moveJetUI(Coordinates originalCoord, Coordinates newCoord, PlayerName player) {
        choosePlayerPanel(player).disableActiveDie();
        gameBoard.moveJetUI(originalCoord, newCoord, player);
    }

    @Override
    public void endGame() {
        setPopupToEnd();
        infoPopup.open();
    }

    private void setPopupToEnd() {
        infoPopup.setButtonToMainMenu();
        infoPopup.setHeaderText("Konec bitvy");
        String winner = gamePresenter.getPlayerOnTurnName() == PlayerName.PLAYER_LEFT
                ? "1" : "2";
        infoPopup.setLabelText("Vyhrává letka velitele " + winner + "!");
        infoPopup.getConfirmButton().removeClassName("displayNone");
    }

    @Override
    public void playKill() {
        soundLibrary.playWithPausedAll(KILL_SOUND);
    }

    @Override
    public void playShoot() {
        soundLibrary.playWithPausedAll(SHOOT_SOUND);
    }

    @Override
    public void playMove() {
        soundLibrary.playWithPausedAll(MOVE_SOUND);
    }

    @Override
    public void playDice() {
        soundLibrary.playWithPausedAll(DICE_SOUND);
    }

    @Override
    public void showPlayerStuckPopup() {
        infoPopup.getConfirmButton().addClassName("displayNone");
        infoPopup.setHeaderText("Pozor!");
        infoPopup.setLabelText("Nelze táhnout žádným letadlem...");
        infoPopup.open();
    }

    @Override
    public void muteSounds() {
        soundLibrary.muteAll();
    }

    @Override
    public void unmuteSounds() {
        soundLibrary.unmuteAll();
    }

    @Override
    public void showTossCoin(boolean tossCoin) {
        tossCoinPopup.open();
        tossCoinPopup.animate(tossCoin);
        soundLibrary.playWithPausedAll(COIN_SOUND);
    }

    @Override
    public void cleanTossCoinPopup() {
        tossCoinPopup.cleanPopup();
    }

    @Override
    public void rollDicesUI(List<Integer> rolls) {
        choosePlayerPanel(gamePresenter.getPlayerOnTurnName()).rollDicesUI(rolls);
    }

    @Override
    public void disableDieByNumber(Integer roll) {
        choosePlayerPanel(gamePresenter.getPlayerOnTurnName()).disableDieByNumber(roll);
    }

    @Override
    public void closeInfoPopup() {
        infoPopup.close();
    }

    @Override
    public boolean isInfoPopupOpen() {
        return infoPopup.isOpened();
    }

    @Override
    public boolean isTossCoinPopupOpen() {
        return tossCoinPopup.isOpened();
    }

    @Override
    public void autoMove(List<PCAction> pcActions) {
        ActionThread actionThread = new ActionThread(UI.getCurrent(), this, gamePresenter, pcActions);
        actionThread.start();
    }

    @Override
    public void openSettingsPopup() {
        settingsPopup.open();
    }
}
