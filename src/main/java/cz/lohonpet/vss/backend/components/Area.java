package cz.lohonpet.vss.backend.components;

/**
 * Enum for displaying components current area of the game
 */
public enum Area {
    NONE, TAKEOFF, ON_SKY
}
