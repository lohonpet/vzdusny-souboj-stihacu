package cz.lohonpet.vss.backend.components;

import cz.lohonpet.vss.ui.components.GameBoxUI;

import java.util.List;

public class EmptyBox extends GameBox {
    EmptyBox(Coordinates coordinates) {
        super(coordinates);
    }

    @Override
    public void setBasicUI(GameBoxUI gameBoxUI) {
    }

    @Override
    public Jet getCurrentJet() {
        return null;
    }

    public boolean isJetAvailable() {
        return false;
    }

    @Override
    public List<Coordinates> countPossibleMoves(Integer roll) {
        return null;
    }

    @Override
    public boolean isPlayable() {
        return false;
    }

    @Override
    public Area getArea() {
        return Area.NONE;
    }

}
