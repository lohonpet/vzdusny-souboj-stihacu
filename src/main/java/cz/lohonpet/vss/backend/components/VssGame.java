package cz.lohonpet.vss.backend.components;

import cz.lohonpet.vss.backend.utils.RandomGenerator;
import cz.lohonpet.vss.ui.components.Action;
import cz.lohonpet.vss.ui.components.Kill;
import cz.lohonpet.vss.ui.components.Move;
import cz.lohonpet.vss.ui.components.Shoot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static cz.lohonpet.vss.backend.components.Area.ON_SKY;
import static cz.lohonpet.vss.backend.components.Area.TAKEOFF;
import static cz.lohonpet.vss.utils.VSSConstants.*;

public class VssGame {
    private final Player playerLeft;
    private final Player playerRight;
    private Player playerOnTurn;
    private final Map<Coordinates, GameBox> boxes = new HashMap<>();

    public VssGame(Player playerLeft, Player playerRight) {
        this.playerLeft = playerLeft;
        this.playerLeft.setPlayerName(PlayerName.PLAYER_LEFT);
        this.playerRight = playerRight;
        this.playerRight.setPlayerName(PlayerName.PLAYER_RIGHT);
        this.playerOnTurn = playerLeft;
        boxesInit();
        jetsStartPositioning(playerLeft);
        jetsStartPositioning(playerRight);
    }

    /**
     * Sets initial coordinations for jets of one player
     *
     * @param player setup for this player
     */
    private void jetsStartPositioning(Player player) {
        int shift = 0;
        int hangarShift = 5;
        if (player == playerRight) {
            shift = BOARD_WIDTH - JETS_START_COLUMNS_RANGE;
            hangarShift = 11;
        }
        int i = 0;
        for (; i < JETS_QUANTITY - JETS_IN_HANGAR_QUANTITY; i++) {
            Coordinates jetCoord = RandomGenerator.randomStartJetCoordinate(shift);
            if (boxes.get(jetCoord).isJetAvailable()) {
                i--;
            } else {
                boxes.get(jetCoord).setCurrentJet(player.getJets().get(i));
            }
        }

        for (int j = 0; j < JETS_IN_HANGAR_QUANTITY; j++) {
            Coordinates hangarCoord = new Coordinates(hangarShift + j, 2);
            boxes.get(hangarCoord).setCurrentJet(player.getJets().get(i++));
        }
    }

    /**
     * Initialize of gameboard in backend
     */
    private void boxesInit() {
        for (int row = BOARD_HEIGHT; row > 0; row--) {
            for (int column = 1; column <= BOARD_WIDTH; column++) {
                Coordinates coordinates = new Coordinates(column, row);
                GameBox tempBox;
                if (row > 5) {
                    tempBox = new SkyBox(coordinates);
                } else if (row > 2) {
                    if (column == 1 || column == 17) {
                        tempBox = new TakeOffBox(coordinates);
                    } else {
                        tempBox = new EmptyBox(coordinates);
                    }
                } else if (row == 2) {
                    if (column == 1 || column == 17) {
                        tempBox = new TakeOffBox(coordinates);
                    } else if ((column >= 5 && column <= 7)
                            || (column >= 11 && column <= 13)) {
                        tempBox = new HangarBox(coordinates);
                    } else if (column == 9) {
                        tempBox = new SettingsBox(coordinates);
                    } else {
                        tempBox = new EmptyBox(coordinates);
                    }
                } else {
                    if (column >= 8 && column <= 10) {
                        tempBox = new EmptyBox(coordinates);
                    } else {
                        tempBox = new TakeOffBox(coordinates);
                    }
                }
                boxes.put(coordinates, tempBox);
            }
        }
    }

    /**
     * Getter for player on turn
     *
     * @return player on turn
     */
    public Player getPlayerOnTurn() {
        return playerOnTurn;
    }

    /**
     * Getter for player NOT on turn
     *
     * @return player NOT on turn
     */
    public Player getPlayerNotOnTurn() {
        return playerOnTurn == playerLeft ? playerRight : playerLeft;
    }

    /**
     * Getter for player name on turn for easier identification
     *
     * @return name of player on turn
     */
    public PlayerName getPlayerOnTurnName() {
        return playerOnTurn.getPlayerName();
    }

    /**
     * Getter for player name NOT on turn for easier identification
     *
     * @return name of player NOT on turn
     */
    public PlayerName getPlayerNotOnTurnName() {
        return getPlayerNotOnTurn().getPlayerName();
    }

    /**
     * Getter for boxes, which represents game boar in backend
     *
     * @return map of boxes
     */
    public Map<Coordinates, GameBox> getBoxes() {
        return boxes;
    }

    /**
     * Gets all coordinates of jets, which can be played in current situation
     * according to throw (if contains 6, it returns takeoff jets coordinates by default, if not, on sky)
     *
     * @param player for who we count it
     * @return List of coordinates of jets which can be played
     */
    public List<Coordinates> getPlayerJetsPlayableCoordinates(Player player) {
        List<Coordinates> coords;
        if (player.getCurrentRolls().contains(6)) {
            coords = getActiveJets(TAKEOFF);
            if (coords.isEmpty()) {
                coords = getActiveJets(ON_SKY);
            }
        } else {
            coords = getActiveJets(ON_SKY);
            if (coords.isEmpty()) {
                coords = getActiveJets(TAKEOFF);
            }
        }
        return coords;
    }

    /**
     * Get coordinates of active jets on given area
     *
     * @param area where to look for active jets
     * @return list of coordinates of active jets
     */
    private List<Coordinates> getActiveJets(Area area) {
        List<Coordinates> coords = new ArrayList<>();
        for (Map.Entry<Coordinates, GameBox> box : boxes.entrySet()) {
            GameBox currentGameBox = box.getValue();
            boolean isCorrectArea = area == currentGameBox.getArea();

            if (isCorrectArea
                    && isJetOfPlayerOnBox(getPlayerOnTurnName(), box.getKey())
                    && currentGameBox.getCurrentJet().isActive()) {
                coords.add(box.getKey());
                currentGameBox.getCurrentJet().activate();
            }
        }
        return coords;
    }

    public List<Coordinates> countPossibleMoves(Coordinates coord, Integer roll) {
        List<Coordinates> possibleCoords = boxes.get(coord).countPossibleMoves(roll);
        possibleCoords = prunePossibleCoords(possibleCoords, coord);
        return possibleCoords;
    }

    /**
     * Get all possibleMoves and remove all, that are not playable, there is not friendly jet
     * on target coordinates and they dont go back from sky to takeoff
     *
     * @param possibleCoords list of possible coordination
     * @param originalCoord  starting point of jet
     * @return list of coordination without removed ones.
     */
    private List<Coordinates> prunePossibleCoords(List<Coordinates> possibleCoords, Coordinates originalCoord) {
        List<Coordinates> newPossCoords = new ArrayList<>();
        GameBox originalBox = boxes.get(originalCoord);

        for (Coordinates singleCoord : possibleCoords) {
            GameBox singleBox = boxes.get(singleCoord);
            if (singleBox.isPlayable()
                    && (!singleBox.isJetAvailable()
                    || singleBox.getCurrentJet().getLeader() != getPlayerOnTurnName())
                    && (originalBox.getArea().ordinal()
                    <= singleBox.getArea().ordinal())) {
                newPossCoords.add(singleCoord);
            }
        }
        return newPossCoords;
    }

    public void moveJet(Coordinates originalCoord, Coordinates newCoord) {
        GameBox originalBox = boxes.get(originalCoord);
        GameBox newBox = boxes.get(newCoord);
        newBox.setCurrentJet(originalBox.getCurrentJet());
        newBox.getCurrentJet().deactivate();
        originalBox.setCurrentJet(null);
    }

    public void switchPlayer() {
        playerOnTurn = getPlayerNotOnTurn();
        playerOnTurn.activeJets();
    }

    // Get coordinates of all jets available for the player
    public List<Coordinates> getJetCoordsOfPlayer(PlayerName playerName) {
        List<Coordinates> coordinates = new ArrayList<>();
        for (Map.Entry<Coordinates, GameBox> box : boxes.entrySet()) {
            if (isJetOfPlayerOnBox(playerName, box.getKey())) {
                coordinates.add(box.getKey());
            }
        }
        return coordinates;
    }

    public List<Coordinates> getJetCoordsOfPlayerOnArea(PlayerName playerName, Area area) {
        List<Coordinates> coordinates = new ArrayList<>();
        for (Coordinates jetCoord : getJetCoordsOfPlayer(playerName)) {
            if (getBoxes().get(jetCoord).getArea() == area) {
                coordinates.add(jetCoord);
            }
        }
        return coordinates;
    }

    public List<Action> createActions(List<Coordinates> possibleCoords) {
        List<Action> actions = new ArrayList<>();
        for (Coordinates oneCoord : possibleCoords) {
            actions.add(createAction(oneCoord));
        }
        return actions;
    }

    public Action createAction(Coordinates targetCoord) {
        if (isJetOfPlayerOnBox(getPlayerNotOnTurnName(), targetCoord)) {
            return new Kill(targetCoord);
        } else {
            List<Coordinates> enemies = countEnemiesInShootingArea(targetCoord);
            if (!enemies.isEmpty()) {
                return new Shoot(targetCoord, enemies);
            }
        }
        return new Move(targetCoord);
    }

    List<Coordinates> countEnemiesInShootingArea(Coordinates oneCoord) {
        List<Coordinates> enemies = new ArrayList<>();
        for (Coordinates oneShoot : countPossibleMoves(oneCoord, SHOOTING_AREA)) {
            if (isJetOfPlayerOnBox(getPlayerNotOnTurnName(), oneShoot)) {
                enemies.add(oneShoot);
            }
        }
        return enemies;
    }

    private boolean isJetOfPlayerOnBox(PlayerName playerName, Coordinates coord) {
        GameBox currentBox = boxes.get(coord);
        if (null == currentBox) {
            System.out.println("[TEMPDEBUG] " + coord + " " + playerName);
        }
        return currentBox.isJetAvailable()
                && currentBox.getCurrentJet().getLeader() == playerName;
    }

    public boolean isEndOfGame() {
        return !isActiveJetOnSkyForPlayer(getPlayerNotOnTurnName());
    }

    private boolean isActiveJetOnSkyForPlayer(PlayerName player) {
        for (Map.Entry<Coordinates, GameBox> box : boxes.entrySet()) {
            if (isJetOfPlayerOnBox(player, box.getKey())
                    && box.getValue().getArea() == ON_SKY) {
                return true;
            }
        }
        return false;
    }

    public PlayerName getOppositePlayer(PlayerName player) {
        return player == PlayerName.PLAYER_LEFT ? PlayerName.PLAYER_RIGHT : PlayerName.PLAYER_LEFT;
    }

    public boolean isEnemyJetOnCoord(Coordinates targetCoordinates, Coordinates originalCoords) {
        return isJetOfPlayerOnBox(getOppositePlayer(
                getBoxes().get(originalCoords).getCurrentJet().getLeader()), targetCoordinates);
    }
}
