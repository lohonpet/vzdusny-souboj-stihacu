package cz.lohonpet.vss.ui.components;

import com.vaadin.flow.component.html.Div;

public class MainMenuItem extends Div {

    public MainMenuItem(String text, String routeAddress) {
        this(text);
        addClickListener( e-> this.getUI().ifPresent(
                ui -> ui.navigate(routeAddress)));
    }

    MainMenuItem(String text) {
        setText(text);
        addClassName("mainMenuItem");
    }


}
