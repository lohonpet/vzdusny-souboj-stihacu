package cz.lohonpet.vss.backend.components;

import cz.lohonpet.vss.ui.components.GameBoxUI;

import java.util.ArrayList;
import java.util.List;

public class SkyBox extends GameBox {

    public SkyBox(Coordinates coordinates) {
        super(coordinates);
    }

    @Override
    public void setBasicUI(GameBoxUI gameBoxUI) {
        gameBoxUI.addPlayable();

    }

    @Override
    public List<Coordinates> countPossibleMoves(Integer roll) {
        Integer newX, newY;
        List<Coordinates> possibleCoords = new ArrayList<>();
        Integer[] possibilities = {0, roll, -roll};
        for (Integer possibleX : possibilities) {
            for (Integer possibleY : possibilities) {
                newX = getX() + possibleX;
                newY = getY() + possibleY;
                if (!(possibleX.equals(possibleY) || possibleX == -possibleY || Coordinates.isOutOfBounds(newX, newY))) {
                    possibleCoords.add(new Coordinates(newX, newY));
                }
            }
        }
        return possibleCoords;
    }

    @Override
    public boolean isPlayable() {
        return true;
    }

    @Override
    public Area getArea() {
        return Area.ON_SKY;
    }


}
