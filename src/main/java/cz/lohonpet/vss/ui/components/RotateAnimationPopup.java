package cz.lohonpet.vss.ui.components;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.shared.Registration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static cz.lohonpet.vss.utils.VSSConstants.PROTOTYPE;

@Component
@Scope(PROTOTYPE)
public class RotateAnimationPopup extends Dialog {
    private final Button confirmButton = new Button();
    private final Div flipper = new Div();
    private final Div front = new Div();
    private final Div back = new Div();
    private Registration regConfirmButton;

    @Autowired
    public RotateAnimationPopup() {
        getElement().setAttribute("theme", "animation-popup");
        Div flipContainer = new Div();
        flipContainer.addClassName("flipContainer");
        flipper.addClassName("flipper");
        front.addClassName("front");
        back.addClassName("back");
        flipper.add(front, back);
        flipContainer.add(flipper);
        confirmButton.addClassNames("heightZero", "tossCoinButton");
        VerticalLayout verticalLayout =
                new VerticalLayout(flipContainer, confirmButton);
        verticalLayout.addClassName("animationPopupVerticalLayout");
        add(verticalLayout);
    }

    public void animate(Boolean hitOrMiss) {
        String shootResult = hitOrMiss ? "Success" : "Missed";
        front.addClassName("front" + shootResult);
        back.addClassName("back" + shootResult);
        flipper.getStyle().set("animation",
                "var(--animateTossCoin");
        confirmButton.getStyle().set("animation",
                "var(--animate" + shootResult + "Button)");
        String buttonText = shootResult.equals("Success") ? "Trefa!" : "Vedle...";
        confirmButton.setText(buttonText);
    }

    public void cleanPopup() {
        removeReg();
        front.removeClassNames("frontSuccess", "frontMissed");
        back.removeClassNames("backSuccess", "backMissed");
        close();
    }

    private void removeReg() {
        if (null != regConfirmButton) {
            regConfirmButton.remove();
            regConfirmButton = null;
        }
    }
}
