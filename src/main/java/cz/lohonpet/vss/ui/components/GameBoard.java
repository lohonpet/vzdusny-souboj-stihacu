package cz.lohonpet.vss.ui.components;

import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.shared.Registration;
import cz.lohonpet.vss.backend.components.Coordinates;
import cz.lohonpet.vss.backend.components.PlayerName;
import cz.lohonpet.vss.backend.presenters.GamePresenterInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

import static cz.lohonpet.vss.utils.VSSConstants.*;

@Component
@Scope(PROTOTYPE)
public class GameBoard extends Composite<HorizontalLayout> implements HasStyle, HasComponents {
    private Map<Coordinates, GameBoxUI> gameBoxesUI;
    private final GamePresenterInterface gamePresenter;

    @Autowired
    public GameBoard(GamePresenterInterface gamePresenter) {
        this.gamePresenter = gamePresenter;
        addClassName("gameBoard");

    }

    public void initBoard() {
        fillBoard();
        makeRoundCorners();
        makeProperBorder();
        showJetsOnStart();
        setSettingsButtons();
    }

    private void setSettingsButtons() {
        GameBoxUI settingsButton = gameBoxesUI.get(new Coordinates(9, 2));
        settingsButton.addClassName("settingsBoard");
        settingsButton.addClickListener(e -> gamePresenter.openSettingsPopup());
    }

    private void showJetsOnStart() {
        List<List<Coordinates>> completeList = gamePresenter.showJetsOnStart();
        for (int i = 0; i < completeList.size(); i++) {
            String side;
            String activity = "";
            if (i == 0) {
                side = leftOrRightJet(PlayerName.PLAYER_LEFT);
            } else {
                side = leftOrRightJet(PlayerName.PLAYER_RIGHT);
                activity = "Inactive";
            }
            for (Coordinates coord : completeList.get(i)) {
                gameBoxesUI.get(coord).addClassNames("jet", side + "Jet" + activity);
            }
        }
    }

    private void fillBoard() {
        gameBoxesUI = gamePresenter.fillBoard();
        for (int row = BOARD_HEIGHT; row > 0; row--) {
            for (int column = 1; column <= BOARD_WIDTH; column++) {
                add(gameBoxesUI.get(new Coordinates(column, row)));
            }
        }
    }

    private void makeProperBorder() {
        for (Map.Entry<Coordinates, GameBoxUI> gameBoxPair : gameBoxesUI.entrySet()) {
            Integer gameBoxX = gameBoxPair.getKey().getX();
            Integer gameBoxY = gameBoxPair.getKey().getY();
            GameBoxUI gameBox = gameBoxPair.getValue();
            if (gameBoxX == 1 || (gameBoxX == 17 && (gameBoxY > 1 && gameBoxY < 6))) {
                gameBox.addClassName("thickYellowBoarderLeft");
            }

            if (gameBoxX == 17 || (gameBoxX == 1 && (gameBoxY > 1 && gameBoxY < 6))) {
                gameBox.addClassName("thickYellowBoarderRight");
            }

            if (gameBoxY == 6 && (gameBoxX > 1 && (gameBoxX < 17)) ||
                    gameBoxY == 1 && (gameBoxX < 7 || (gameBoxX > 10))) {
                gameBox.addClassName("thickYellowBoarderBottom");
            }

            if (gameBoxY == 13 || (gameBoxY == 1 && (gameBoxX > 1 && (gameBoxX < 5))) ||
                    (gameBoxY == 1 && (gameBoxX > 13 && (gameBoxX < 17))) ||
                    (gameBoxY == 2 && (gameBoxX == 6 || (gameBoxX == 12)))) {
                gameBox.addClassName("thickYellowBoarderTop");
            }
        }
    }

    private void makeRoundCorners() {
        for (Map.Entry<Coordinates, GameBoxUI> gameBoxPair : gameBoxesUI.entrySet()) {
            Integer gameBoxX = gameBoxPair.getKey().getX();
            Integer gameBoxY = gameBoxPair.getKey().getY();
            GameBoxUI gameBox = gameBoxPair.getValue();
            if ((gameBoxX == 1 && gameBoxY == 13) ||
                    (gameBoxX == 5 && gameBoxY == 2) ||
                    (gameBoxX == 11 && gameBoxY == 2)) {
                gameBox.addClassNames("borderRadiusTopLeft", "thickYellowBoarderLeft", "thickYellowBoarderTop");
            }
            if ((gameBoxX == 17 && gameBoxY == 13) ||
                    (gameBoxX == 7 && gameBoxY == 2) ||
                    (gameBoxX == 13 && gameBoxY == 2)) {
                gameBox.addClassNames("borderRadiusTopRight", "thickYellowBoarderRight", "thickYellowBoarderTop");
            }

            if ((gameBoxX == 1 && gameBoxY == 1) ||
                    (gameBoxX == 11 && gameBoxY == 1)) {
                gameBox.addClassNames("borderRadiusBottomLeft", "thickYellowBoarderLeft", "thickYellowBoarderBottom");
            }

            if ((gameBoxX == 7 && gameBoxY == 1) ||
                    (gameBoxX == 17 && gameBoxY == 1)) {
                gameBox.addClassNames("borderRadiusBottomRight", "thickYellowBoarderRight", "thickYellowBoarderBottom");
            }
        }
    }

    private void setHoverListeners(List<Coordinates> jetCoords, Integer roll) {
        for (Coordinates oneCoord : jetCoords) {
            List<Action> possibleActions = gamePresenter.countPossibleMoves(oneCoord, roll);
            GameBoxUI currentGameBox = gameBoxesUI.get(oneCoord);

            //after hover on jet
            Registration hoverReg = currentGameBox.getElement().addEventListener(
                    "mouseover", e -> {
                        removePossibleRegistrations();
                        reHighlight(oneCoord, possibleActions);
                        setMovePossibleListener(oneCoord, possibleActions);
                    });
            currentGameBox.setHoverRegistrations(hoverReg);
        }
    }

    private void removePossibleRegistrations() {
        for (Map.Entry<Coordinates, GameBoxUI> gameBoxPair : gameBoxesUI.entrySet()) {
            gameBoxPair.getValue().removePossibleRegistration();
        }
    }

    private void removeAllRegistrations() {
        for (Map.Entry<Coordinates, GameBoxUI> gameBoxPair : gameBoxesUI.entrySet()) {
            gameBoxPair.getValue().removeAllRegistrations();
        }
    }

    private void setMovePossibleListener(Coordinates originalCoord, List<Action> possibleActions) {
        for (Action oneAction : possibleActions) {
            GameBoxUI currentGameBox = gameBoxesUI.get(oneAction.getTargetCoordinates());
            Registration possibleRegistration = currentGameBox.addClickListener(e -> {
                unsetHighlight();
                removeAllRegistrations();
                oneAction.makeAction(gamePresenter, originalCoord);
            });
            currentGameBox.setPossibleRegistrations(possibleRegistration);
        }
    }

    private void reHighlight(Coordinates coord, List<Action> possibleActions) {
        unsetHighlight();
        setHighlightToBox(coord);
        setActionToList(possibleActions);
    }

    private void unsetHighlight() {
        for (Map.Entry<Coordinates, GameBoxUI> gameBoxPair : gameBoxesUI.entrySet()) {
            GameBoxUI currentBox = gameBoxPair.getValue();
            currentBox.removeClassNames(HIGHLIGHT, "crosshair", "leftJetFlame", "rightJetFlame");
        }
    }

    private void setActionToBox(Action action) {
        action.highlightBox(gameBoxesUI.get(action.getTargetCoordinates()));
    }


    private void setActionToList(List<Action> possibleActions) {
        for (Action oneAction : possibleActions) {
            setActionToBox(oneAction);
        }
    }

    private void setHighlightToBox(Coordinates coord) {
        gameBoxesUI.get(coord).addClassName(HIGHLIGHT);
    }

    private void setHighlightToList(List<Coordinates> highlightCoord) {
        for (Coordinates oneCoord : highlightCoord) {
            setHighlightToBox(oneCoord);
        }
    }

    public void setJetsAfterClickDie(List<Coordinates> jetCoords, Integer roll) {
        unsetHighlight();
        removeAllRegistrations();
        setHighlightToList(jetCoords);
        setHoverListeners(jetCoords, roll);
    }

    public void deactiveJetsByPlayer(PlayerName player, List<Coordinates> jetCoordsByPlayer) {
        String side = leftOrRightJet(player);
        for (Coordinates jetCoord : jetCoordsByPlayer) {
            gameBoxesUI.get(jetCoord).deactivateJetUI(side);
        }
    }

    private String leftOrRightJet(PlayerName player) {
        return player == PlayerName.PLAYER_LEFT ? "left" : "right";
    }

    public void activeJetsByPlayer(PlayerName player, List<Coordinates> jetCoordsByPlayer) {
        String side = leftOrRightJet(player);
        for (Coordinates jetCoord : jetCoordsByPlayer) {
            gameBoxesUI.get(jetCoord).activateJetUI(side);
        }
    }

    public void moveJetUI(Coordinates originalCoord, Coordinates newCoord,
                          PlayerName player) {
        String side = leftOrRightJet(player);
        gameBoxesUI.get(originalCoord).removeClassNames("jet", side + "Jet", side + "JetInactive");
        gameBoxesUI.get(newCoord).removeClassNames("leftJetInactive", "rightJetInactive");
        gameBoxesUI.get(newCoord).addClassNames("jet", side + "JetInactive");
    }
}
