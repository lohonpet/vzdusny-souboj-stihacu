package cz.lohonpet.vss.ui.views;

import cz.lohonpet.vss.backend.components.Coordinates;
import cz.lohonpet.vss.backend.components.PCAction;
import cz.lohonpet.vss.backend.components.PlayerName;
import cz.lohonpet.vss.ui.components.DieUI;

import java.util.List;

public interface GameViewInterface {
    void rollDicesUI(List<Integer> rolls);

    void autoMove(List<PCAction> pcActions);

    void setJetsAfterClickDie(List<Coordinates> jetCoords, Integer roll);

    void openSettingsPopup();

    void showTossCoin(boolean tossCoin);

    void unmuteSounds();

    void muteSounds();

    void showPlayerStuckPopup();

    void playDice();

    void playMove();

    void playKill();

    void endGame();

    List<DieUI> getActiveDice();

    void activatePlayerUI(PlayerName player, List<Coordinates> jetCoordsByPlayer);

    void deactivatePlayerUI(PlayerName player, List<Coordinates> jetCoordsByPlayer);

    boolean areAllDiceUsed(PlayerName player);

    void cleanTossCoinPopup();

    void moveJetUI(Coordinates originalCoord, Coordinates newCoord, PlayerName player);

    void playShoot();

    void disableDieByNumber(Integer roll);

    void closeInfoPopup();

    boolean isInfoPopupOpen();

    boolean isTossCoinPopupOpen();
}
