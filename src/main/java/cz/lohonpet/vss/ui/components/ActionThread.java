package cz.lohonpet.vss.ui.components;

import com.vaadin.flow.component.UI;
import cz.lohonpet.vss.backend.components.PCAction;
import cz.lohonpet.vss.backend.presenters.GamePresenterInterface;
import cz.lohonpet.vss.ui.views.GameViewInterface;

import java.util.List;

public class ActionThread extends Thread {
    private final UI ui;
    private final GameViewInterface gameView;
    private final GamePresenterInterface gamePresenter;
    private final List<PCAction> pcActions;

    public ActionThread(UI ui, GameViewInterface view, GamePresenterInterface gamePresenter, List<PCAction> pcActions) {
        this.ui = ui;
        this.gameView = view;
        this.gamePresenter = gamePresenter;
        this.pcActions = pcActions;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(1800);
            if (gameView.isInfoPopupOpen()) {
                Thread.sleep(1800);
            }
            for (PCAction pcAction : pcActions) {
                if (gamePresenter.isEndOfGame()) {
                    break;
                }
                Thread.sleep(2200);
                if (gameView.isTossCoinPopupOpen()) {
                    Thread.sleep(2500);
                }
                ui.access(() -> {
                    gameView.disableDieByNumber(pcAction.getRoll());
                    pcAction.makeAction(gamePresenter);
                });
            }
        } catch (
                InterruptedException e) {
            e.printStackTrace();

        }

    }
}
