package cz.lohonpet.vss.ui.components;

import cz.lohonpet.vss.backend.components.Coordinates;
import cz.lohonpet.vss.backend.presenters.GamePresenterInterface;

import static cz.lohonpet.vss.utils.VSSConstants.HIGHLIGHT;

public class Move extends Action {
    public Move(Coordinates targetCoordinates) {
        super(targetCoordinates);
    }

    @Override
    public void highlightBox(GameBoxUI gameBoxUI) {
        gameBoxUI.addClassName(HIGHLIGHT);
    }

    @Override
    public void makeAction(GamePresenterInterface gamePresenter, Coordinates originalCoord) {
        gamePresenter.moveJetWithSoundAndSwitch(originalCoord, getTargetCoordinates());
    }

    @Override
    public boolean isAttackStillPossible(GamePresenterInterface gamePresenter, Coordinates originalCoords) {
        return true;
    }

    /**
     * Substitue action for move is the same as original action
     *
     * @param gamePresenter  presenter to use
     * @param originalCoords coords, where the jet with action is
     */
    @Override
    public void makeSubstituteAction(GamePresenterInterface gamePresenter, Coordinates originalCoords) {
        makeAction(gamePresenter, originalCoords);
    }
}
