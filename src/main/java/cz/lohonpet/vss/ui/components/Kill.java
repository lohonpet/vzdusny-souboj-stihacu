package cz.lohonpet.vss.ui.components;

import cz.lohonpet.vss.backend.components.Coordinates;
import cz.lohonpet.vss.backend.presenters.GamePresenterInterface;

import static cz.lohonpet.vss.utils.VSSConstants.*;

public class Kill extends Action {
    public Kill(Coordinates targetCoordinates) {
        super(targetCoordinates);
    }

    @Override
    public void highlightBox(GameBoxUI gameBoxUI) {
        String side = gameBoxUI.hasClassName("leftJetInactive") ? "left" : "right";
        gameBoxUI.addClassNames(HIGHLIGHT, side + "JetFlame");
    }

    @Override
    public void makeAction(GamePresenterInterface gamePresenter, Coordinates originalCoord) {
        gamePresenter.killEnemyWithSound(originalCoord, getTargetCoordinates());
    }

    @Override
    public boolean isAttackStillPossible(GamePresenterInterface gamePresenter, Coordinates originalCoords) {
        return gamePresenter.isEnemyJetOnCoord(getTargetCoordinates(), originalCoords);
    }

    @Override
    public void makeSubstituteAction(GamePresenterInterface gamePresenter, Coordinates originalCoords) {
        gamePresenter.trySwitchPlayer();
    }
}
