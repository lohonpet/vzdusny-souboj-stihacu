package cz.lohonpet.vss.ui.components;

import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Paragraph;

public class MainMenuItemWithPopup extends MainMenuItem {
    private final InfoPopup infoPopup = new InfoPopup();

    public MainMenuItemWithPopup(String text) {
        super(text);
    }

    public void setHeaderText(String text) {
        infoPopup.setHeaderText(text);
    }

    public void openPopup() {
        infoPopup.open();
    }

    public void addH3WithText(String headerText) {
        infoPopup.getContent().add(new H3(headerText));
    }

    public void addParagraphWithText(String headerText) {
        infoPopup.getContent().add(new Paragraph(headerText));
    }

    public void setButtonToMainMenu() {
        infoPopup.setButtonToMainMenu();
    }

    public void setLabelHtml(String html) {
        infoPopup.setContentHtml(html);
    }

    public void addInfoPopupContentClass(String className) {
        infoPopup.getContent().addClassName(className);
    }
}
