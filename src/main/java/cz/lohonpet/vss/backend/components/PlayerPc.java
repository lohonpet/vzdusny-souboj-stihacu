package cz.lohonpet.vss.backend.components;

import org.paukov.combinatorics3.Generator;

import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static cz.lohonpet.vss.backend.components.Area.ON_SKY;
import static cz.lohonpet.vss.backend.components.Area.TAKEOFF;
import static cz.lohonpet.vss.utils.VSSConstants.*;
import static java.lang.Math.abs;

public class PlayerPc extends Player {
    private static final Logger LOGGER = Logger.getLogger(PlayerPc.class.getName());

    private List<Coordinates> pcCurrentCoords;
    private List<Coordinates> enemyOnSkyJetsCoords;
    private List<Coordinates> enemyTakeOffJetsCoords;
    private VssGame game;
    private List<List<PossibleMove>> possibleMoves;
    //TODO Remove, only for debugging?
    private Integer moveCounter = 0;


    private class PossibleMove {
        private final Coordinates jetCoord;
        private final Integer roll;
        private List<Coordinates> possibleCoordinates;
        private Integer topPriority;
        private Coordinates targetCoord;
        private final Integer moveNumber;

        PossibleMove(Coordinates jetCoord, Integer roll) {
            this.jetCoord = jetCoord;
            this.roll = roll;
            possibleCoordinates = null;
            targetCoord = null;
            topPriority = null;
            this.moveNumber = moveCounter++;
        }

        public PossibleMove clone() {
            PossibleMove newPossMove = new PossibleMove(this.jetCoord, this.roll);
            newPossMove.possibleCoordinates = this.possibleCoordinates;
            newPossMove.targetCoord = this.targetCoord;
            newPossMove.topPriority = this.topPriority;
            return newPossMove;
        }

        void computeBestMove() {
            for (Coordinates possCoord : possibleCoordinates) {
                Integer tempPriority = computePriority(possCoord);
                if (null == topPriority || topPriority < tempPriority) {
                    topPriority = tempPriority;
                    targetCoord = possCoord;
                }
            }
            //LOGGER.info(this::toString);
        }

        private Integer computePriority(Coordinates possCoord) {
            LOGGER.info("\nCOMPUTING PRIORITY FOR MOVE: " + moveNumber + " from: " + jetCoord + " to: " + possCoord);
            Integer priority = 0;
            if (game.getBoxes().get(possCoord).getArea() == ON_SKY) {
                priority += computeKillEnemy(possCoord);
                LOGGER.info("Priority after KillEnemy: " + priority);
                priority += computeMoveTowardsEnemy(possCoord);
                LOGGER.info("Priority after MoveTowardsEnemy: " + priority);
                priority += computeAgressionFactor();
                LOGGER.info("Priority after AgressionFactor: " + priority);

                priority += computeDefendDirect(possCoord);
                LOGGER.info("Priority after DefendDirect: " + priority);
                priority += computeDefendAim(possCoord);
                LOGGER.info("Priority after DefendAim: " + priority);

                priority += computeRunAway(possCoord);
                LOGGER.info("Priority after RunAway: " + priority);

                priority += takeOffBonus();
                LOGGER.info("Priority after takeoffBonus: " + priority);

            } else {
                priority += computeTakeOff(possCoord);
                LOGGER.info("Priority after takeoff: " + priority);
            }
            return priority;
        }

        private Integer computeTakeOff(Coordinates possCoord) {
            return possCoord.getY() * TAKEOFF_PRIORITY;
        }

        private Integer takeOffBonus() {
            return (game.getBoxes().get(jetCoord).getArea() == TAKEOFF) ? TAKEOFF_BONUS : 0;
        }

        private Integer computeAgressionFactor() {
            Integer differenceJetsAmount = game.getJetCoordsOfPlayerOnArea(PlayerPc.this.playerName, ON_SKY).size()
                    - enemyOnSkyJetsCoords.size();
            return differenceJetsAmount > 0 ? differenceJetsAmount * AGGRESSION_FACTOR_PRIORITY : 0;
        }

        private Integer computeRunAway(Coordinates possCoord) {
            if ((enemyOnSkyJetsCoords.size()
                    - game.getJetCoordsOfPlayerOnArea(PlayerPc.this.playerName, ON_SKY).size() >= 2)
                    && game.getJetCoordsOfPlayerOnArea(PlayerPc.this.playerName, TAKEOFF).size() > 0) {
                return computeMoveTowardsEnemy(possCoord) * -1;
            }
            return 0;
        }

        private Integer computeMoveTowardsEnemy(Coordinates possCoord) {
            Integer priorityMoving = 0;
            for (Coordinates enemyCoord : enemyOnSkyJetsCoords) {
                int distanceOriginal = jetCoord.distanceFrom(enemyCoord);
                int distanceTarget = possCoord.distanceFrom(enemyCoord);
                if (distanceOriginal > distanceTarget) {
                    priorityMoving++;
                }
            }

            return priorityMoving * MOVE_PRIORITY;
        }

        private Integer computeDefendAim(Coordinates possCoord) {
            return getAmountOfEnemiesAim(1, 5 + (hasEnemyAllJetsOnTakeoff() ? 0 : 1), possCoord) * AIM_DEFEND_PRIORITY;
        }

        private Integer computeDefendDirect(Coordinates possCoord) {
            //enemy cant use six for direct attack
            int sixDirect = hasEnemyAllJetsOnTakeoff() ? 0 : 1;
            int defendPriority = 0;

            //TODO Probabilities for more attackers?
            // Distance 1
            Integer distanceStartEnemies = getAmountOfEnemiesDirect(1, 1, possCoord);
            if (distanceStartEnemies > 0) {
                //TODO Probabilities for more attackers PRECISE
                defendPriority += distanceStartEnemies * START_DEFEND_PRIORITY;
            }

            // Distance 2-5
            Integer distanceMiddleEnemies = getAmountOfEnemiesDirect(2, 4 + sixDirect, possCoord);
            if (distanceMiddleEnemies > 0) {
                //TODO Probabilities for more attackers PRECISE
                defendPriority += MIDDLE_DEFEND_PRIORITY + 10 * (distanceMiddleEnemies - 1);
            }

            Integer distanceEndEnemies = getAmountOfEnemiesDirect(5 + sixDirect, 5 + sixDirect, possCoord);
            if (distanceEndEnemies > 0) {
                //TODO Probabilities for more attackers PRECISE
                defendPriority += END_DEFEND_PRIORITY + 10 * (distanceEndEnemies - 1);
            }
            return defendPriority;
        }


        private Integer computeKillEnemy(Coordinates possCoord) {
            GameBox possibleBox = game.getBoxes().get(possCoord);
            if (possibleBox.isEnemyJetAvailable(PlayerPc.this.playerName)) {
                return KILL_PRIORITY;
            }
            List<Coordinates> enemies = game.countEnemiesInShootingArea(possCoord);
            if (!enemies.isEmpty()) {
                int enemiesAmount = enemies.size();
                int aimPriority = (int) (((double) enemiesAmount / (enemiesAmount + 1)) * AIM_PRIORITY);
                LOGGER.info("Priority from aim: " + aimPriority);
                return aimPriority;
            }
            return 0;
        }

        @Override
        public String toString() {
            StringBuilder result = new StringBuilder("\nMove #" + moveNumber + " from: " + jetCoord.toString()
                    + " roll: " + roll + "\n");

            result.append("Top priority:").append(topPriority).append("\n");

            result.append("Target coordinations:").append(targetCoord).append("\n");

            if (null != possibleCoordinates) {
                result.append("PossibleCoords:\n");
                for (Coordinates onePossCord : possibleCoordinates) {
                    result.append(onePossCord).append("\n");
                }
            }
            return result.toString();
        }
    }

    private class SortAllMovesByPriority implements Comparator<List<PossibleMove>> {
        @Override
        public int compare(List<PossibleMove> x, List<PossibleMove> y) {
            Integer xTotalPriority = 0;
            Integer yTotalPriority = 0;
            for (PossibleMove oneX : x) {
                if (null != oneX.topPriority) {
                    xTotalPriority += oneX.topPriority;
                }
            }
            for (PossibleMove oneY : y) {
                if (null != oneY.topPriority) {
                    yTotalPriority += oneY.topPriority;
                }
            }
            return yTotalPriority - xTotalPriority;
        }
    }

    private class SortOptionByPriority implements Comparator<PossibleMove> {

        @Override
        public int compare(PossibleMove o1, PossibleMove o2) {
            if (null == o2.topPriority) {
                return -1;
            } else if (null == o1.topPriority) {
                return 1;
            }
            return o2.topPriority - o1.topPriority;
        }
    }

    private Integer getAmountOfEnemiesDirect(int from, int to, Coordinates possCoord) {
        return getAmountOfEnemiesInDistance(from, to, 0, possCoord);
    }

    private Integer getAmountOfEnemiesAim(int from, int to, Coordinates possCoord) {
        return getAmountOfEnemiesInDistance(from, to, 1, possCoord);
    }

    private Integer getAmountOfEnemiesInDistance(int from, int to, int shift, Coordinates possCoord) {
        Integer enemiesInDistance = 0;
        for (Coordinates enemyCoord : enemyOnSkyJetsCoords) {
            int xDiff = abs(enemyCoord.getX() - possCoord.getX());
            int yDiff = abs(enemyCoord.getY() - possCoord.getY());
            if (((xDiff <= to && xDiff >= from) && (yDiff == shift || yDiff == -shift))
                    || ((yDiff <= to && yDiff >= from) && (xDiff == shift || xDiff == -shift))) {
                enemiesInDistance++;
            }
        }
        return enemiesInDistance;
    }

    private boolean hasEnemyAllJetsOnTakeoff() {
        Integer enemiesOnTakeoff = 0;
        for (Coordinates enemyCoord : enemyTakeOffJetsCoords) {
            enemiesOnTakeoff++;
        }
        return enemiesOnTakeoff.equals(JETS_IN_HANGAR_QUANTITY);
    }

    @Override
    public List<PCAction> computeMoves(VssGame game) {
        this.game = game;
        pcCurrentCoords = this.game.getPlayerJetsPlayableCoordinates(this);
        enemyOnSkyJetsCoords = this.game.getJetCoordsOfPlayerOnArea(game.getOppositePlayer(this.playerName), ON_SKY);
        enemyTakeOffJetsCoords = this.game.getJetCoordsOfPlayerOnArea(game.getOppositePlayer(this.playerName), TAKEOFF);
        List<List<Coordinates>> allJetsVariations = computeVariationsOfJets();
        possibleMoves = computePossibleMovesPermutation(allJetsVariations);
        computeAndEvaluatePossibleMoves();
        sortPossibleMoves();
        checkBestOption();
        printBestOption();
        return fillPCActions();
    }

    private void printBestOption() {
        for (PossibleMove oneMove : possibleMoves.get(0)) {
            LOGGER.info(oneMove.toString());
        }
    }

    private List<PCAction> fillPCActions() {
        List<PCAction> pcActions = new ArrayList<>();
        for (PossibleMove oneMove : possibleMoves.get(0)) {
            pcActions.add(new PCAction(game.createAction(oneMove.targetCoord), oneMove.roll, oneMove.jetCoord));
        }
        return pcActions;
    }

    private void checkBestOption() {
        Iterator<List<PossibleMove>> i = possibleMoves.iterator();
        while (i.hasNext()) {
            List<PossibleMove> tempList = i.next();
            if (isTheSameTarget(tempList) || isTargetNull(tempList)) {
                if (i.hasNext()) {
                    i.remove();
                } else {
                    pruneLastMove();
                    break;
                }
            }
        }
    }

    private boolean isTargetNull(List<PossibleMove> tempList) {
        for (PossibleMove oneMove : tempList) {
            if (null == oneMove.targetCoord) {
                return true;
            }
        }
        return false;
    }

    //TODO rewrite this method
    private void pruneLastMove() {
        List<PossibleMove> original = possibleMoves.get(0);
        List<PossibleMove> newTurn = new ArrayList<>();
        for (PossibleMove oneMove : original) {
            if (null == oneMove.targetCoord) {
                break;
            }
            boolean duplicite = false;
            for (PossibleMove oneMoveOther : original) {
                if (oneMove.targetCoord.equals(oneMoveOther.targetCoord)
                        && oneMove != oneMoveOther) {
                    duplicite = true;
                    if (!newTurn.contains(oneMove) && !newTurn.contains(oneMoveOther)) {
                        newTurn.add(oneMove);
                    }
                    break;
                }
            }
            if (!duplicite) {
                newTurn.add(oneMove);
            }
        }
        possibleMoves.clear();
        possibleMoves.add(newTurn);
    }

    private boolean isTheSameTarget(List<PossibleMove> possibleBest) {
        for (PossibleMove oneMove : possibleBest) {
            for (PossibleMove oneMoveOther : possibleBest) {
                if (Objects.equals(oneMove.targetCoord, oneMoveOther.targetCoord)
                        && oneMove != oneMoveOther) {
                    return true;
                }
            }
        }
        return false;
    }


    private void sortPossibleMoves() {
        possibleMoves.sort(new SortAllMovesByPriority());
    }

    private void computeAndEvaluatePossibleMoves() {
        for (List<PossibleMove> onePossMovesList : possibleMoves) {
            for (PossibleMove onePossMove : onePossMovesList) {
                onePossMove.possibleCoordinates = game.countPossibleMoves(onePossMove.jetCoord, onePossMove.roll);
                onePossMove.computeBestMove();
            }
        }
        List<List<PossibleMove>> additionalMoves = new ArrayList<>();
        for (List<PossibleMove> onePossMovesList : possibleMoves) {
            for (PossibleMove onePossMove : onePossMovesList) {
                if (onePossMove.possibleCoordinates.isEmpty()) {
                    additionalMoves.add(tryAddCoordinatesOfJets(onePossMove, onePossMovesList));
                }
            }
        }
        possibleMoves.addAll(additionalMoves);
    }

    private List<PossibleMove> tryAddCoordinatesOfJets(PossibleMove onePossMove, List<PossibleMove> onePossMovesList) {
        List<Coordinates> possibleCoords = game.getBoxes().get(onePossMove.jetCoord).countPossibleMoves(onePossMove.roll);
        List<Coordinates> newPossCoords = new ArrayList<>();
        List<PossibleMove> newPossibleMoves = new ArrayList<>();

        for (Coordinates onePossCord : possibleCoords) {
            GameBox singleBox = game.getBoxes().get(onePossCord);
            if (singleBox.isPlayable()
                    && (game.getBoxes().get(onePossMove.jetCoord).getArea().ordinal()
                    <= singleBox.getArea().ordinal())) {
                newPossCoords.add(onePossCord);
            }
        }
        for (PossibleMove oneMove : onePossMovesList) {
            if (newPossCoords.contains(oneMove.jetCoord)) {
                for (PossibleMove clonedMove : onePossMovesList) {
                    PossibleMove newClonedMove = clonedMove.clone();
                    if (onePossMove == clonedMove) {
                        newClonedMove.targetCoord = oneMove.jetCoord;
                        newClonedMove.topPriority = LAST_MOVE_PRIORITY;
                    }
                    newPossibleMoves.add(newClonedMove);
                }
                newPossibleMoves.sort(new SortOptionByPriority());
            }
        }
        return newPossibleMoves;

    }

    private List<List<PossibleMove>> computePossibleMovesPermutation(List<List<Coordinates>> allJetsVariations) {
        List<List<PossibleMove>> jetCoordRollList = new ArrayList<>();
        List<List<Integer>> rollsCombinations = Generator
                .permutation(currentRolls)
                .simple()
                .stream()
                .collect(Collectors.toList());

        for (List<Coordinates> allJetsVariation : allJetsVariations) {
            for (List<Integer> rollsCombination : rollsCombinations) {
                List<PossibleMove> oneJetCoordRollsList = new ArrayList<>();
                for (int i = 0; i < allJetsVariation.size(); i++) {
                    oneJetCoordRollsList.add(
                            new PossibleMove(allJetsVariation.get(i), rollsCombination.get(i)));
                }
                jetCoordRollList.add(oneJetCoordRollsList);
            }
        }
        LOGGER.info("Possible roll and coords trinity's are " + jetCoordRollList.size());

        return jetCoordRollList;
    }

    private List<List<Coordinates>> computeVariationsOfJets() {
        List<List<Coordinates>> allJets = new ArrayList<>();
        Integer combLength = pcCurrentCoords.size() >= DICE_COUNT ? DICE_COUNT : pcCurrentCoords.size();
        Generator.combination(pcCurrentCoords)
                .simple(combLength)
                .stream()
                .forEach(allJets::add);

        if (!allJets.isEmpty() && combLength < DICE_COUNT) {
            List<Coordinates> originalJets = allJets.get(0);
            Area inactiveArea = game.getBoxes().get(originalJets.get(0)).getArea()
                    == ON_SKY ? TAKEOFF : ON_SKY;
            List<List<Coordinates>> addonJetsCombinations = new ArrayList<>();

            List<Coordinates> additionalJets = game.getJetCoordsOfPlayerOnArea(playerName, inactiveArea);
            int additionalCombLength = DICE_COUNT - combLength;
            if (additionalCombLength > additionalJets.size()) {
                additionalCombLength = additionalJets.size();
            }
            Generator.combination(additionalJets)
                    .simple(additionalCombLength)
                    .stream()
                    .forEach(addonJetsCombinations::add);

            if (!addonJetsCombinations.isEmpty()) {
                List<List<Coordinates>> finalList = new ArrayList<>();
                for (List<Coordinates> oneAddonComb : addonJetsCombinations) {
                    List<Coordinates> tempOneFinalList = new ArrayList<>(originalJets);
                    tempOneFinalList.addAll(oneAddonComb);
                    finalList.add(tempOneFinalList);
                }
                allJets = finalList;
            }
        }
        LOGGER.info("Possible variations of jets is " + allJets.size() + "jetsNumber: " + allJets.get(0).size());
        return allJets;
    }

    @Override
    public boolean needsAutoTurn() {
        return true;
    }
}
