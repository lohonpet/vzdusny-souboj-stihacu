package cz.lohonpet.vss.ui.components;

import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.shared.Registration;
import cz.lohonpet.vss.backend.components.Activity;

public class DieUI extends Div implements HasStyle {
    private final String background;
    private Integer roll;
    private Registration clickRegistration;
    private Activity activity = Activity.ACTIVE;

    public DieUI(String background) {
        this.background = background;
        activateAsEmpty();
    }

    public void setClickRegistration(Registration clickRegistration) {
        this.clickRegistration = clickRegistration;
    }

    public void removeClickRegistration() {
        if (clickRegistration != null) {
            clickRegistration.remove();
            clickRegistration = null;
        }
    }

    public void animate() {
        this.getStyle().set("animation",
                "var(--animate" + background + "), show" + background + roll + " var(--animate" + background + "Finish)");
    }

    public void setRoll(Integer roll) {
        this.roll = roll;
    }

    public void deactivate() {
        removeClickRegistration();
        removeClassName("activeDie");
        addClassNames("deactivate", "deactivate" + background);
        getStyle().remove("animation");
        activity = Activity.INACTIVE;
    }

    public Integer getRoll() {
        return roll;
    }

    public boolean isActive() {
        return activity == Activity.ACTIVE;
    }

    public void activate() {
        removeClassNames("deactivate", "deactivate" + background);
        activateAsEmpty();
        activity = Activity.ACTIVE;
    }

    private void activateAsEmpty() {
        addClassNames("die", this.background.toLowerCase() + "DieEmpty");
    }


    void removeHover() {
        addClassName("dieNoHoverScale");
    }
}
