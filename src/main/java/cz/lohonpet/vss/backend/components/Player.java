package cz.lohonpet.vss.backend.components;

import cz.lohonpet.vss.backend.utils.RandomGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static cz.lohonpet.vss.utils.VSSConstants.JETS_QUANTITY;
import static cz.lohonpet.vss.utils.VSSConstants.PROTOTYPE;

@Service
@Scope(PROTOTYPE)
public class Player {
    List<Integer> currentRolls;
    private final List<Jet> jets = new ArrayList<>();
    PlayerName playerName;

    @Autowired
    public Player() {
        jetsInit();
    }

    private void jetsInit() {
        for (int i = 0; i < JETS_QUANTITY; i++) {
            jets.add(new Jet());
        }
    }

    public void setPlayerName(PlayerName playerName) {
        this.playerName = playerName;
        for (Jet jet : jets) {
            jet.setLeader(this.playerName);
        }
    }

    public PlayerName getPlayerName() {
        return playerName;
    }

    public List<Jet> getJets() {
        return jets;
    }

    public List<Integer> getCurrentRolls() {
        return currentRolls;
    }

    public void deactiveJets() {
        for (Jet jet : jets) {
            jet.deactivate();
        }
    }

    public void activeJets() {
        for (Jet jet : jets) {
            jet.activate();
        }
    }

    public List<Integer> rollDices() {
        currentRolls = RandomGenerator.rollNTimesDiceD6(3);
        return currentRolls;
    }

    public List<PCAction> computeMoves(VssGame game) {
        return null;
    }

    public boolean needsAutoTurn() {
        return false;
    }
}
