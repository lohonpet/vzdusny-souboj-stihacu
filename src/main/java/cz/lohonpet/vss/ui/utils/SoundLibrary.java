package cz.lohonpet.vss.ui.utils;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.server.VaadinServlet;
import com.vaadin.flow.spring.annotation.UIScope;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;


import java.io.File;
import java.util.HashMap;
import java.util.Map;

import static cz.lohonpet.vss.utils.VSSConstants.*;

@org.springframework.stereotype.Component
@UIScope
public class SoundLibrary extends VerticalLayout implements HasComponents {

    private final Map<String, Sound> sounds;
    private boolean isMuted = false;

    @Autowired
    public SoundLibrary() {
        sounds = new HashMap<>();
    }

    public void init() {
        if (sounds.isEmpty()) {
            File soundsDir = new File(ServletUtils.getBaseDirectory(VaadinServlet.getCurrent())
                    + SOUNDS_DIRECTORY);
            File[] soundFiles = soundsDir.listFiles();
            if (soundFiles != null) {
                for (File oneSoundFile : soundFiles) {
                    String nameWithoutExtension =
                            FilenameUtils.removeExtension(oneSoundFile.getName());
                    String soundFileNameWithRelativePath =
                            oneSoundFile.getPath().substring(
                                    oneSoundFile.getPath().lastIndexOf(
                                            WEBAPP_DIRECTORY) + (WEBAPP_DIRECTORY.length() + 1));
                    sounds.put(nameWithoutExtension,
                            new Sound(soundFileNameWithRelativePath));
                    add(sounds.get(nameWithoutExtension));
                }
            }
        }
    }

    public void muteAll() {
        for (Map.Entry<String, Sound> soundPair : sounds.entrySet()) {
            soundPair.getValue().mute();
        }
        isMuted = true;
    }

    private void play(String soundName) {
        sounds.get(soundName).play();
    }

    public void playWithPausedAll(String soundName) {
        pauseAll();
        load(soundName);
        play(soundName);
    }

    public void setLoopandAutoplay(String soundName) {
        sounds.get(soundName).setLoop();
        sounds.get(soundName).setAutoplay();
        load(soundName);
        play(soundName);
    }

    private void load(String soundName) {
        sounds.get(soundName).load();
    }

    private void pause(String soundName) {
        sounds.get(soundName).pause();
    }

    private void pauseAll() {
        for (Map.Entry<String, Sound> soundPair : sounds.entrySet()) {
            pause(soundPair.getKey());
        }
    }

    public void unmuteAll() {
        for (Map.Entry<String, Sound> soundPair : sounds.entrySet()) {
            soundPair.getValue().unmute();
        }
        isMuted = false;
    }

    public boolean isMuted() {
        return isMuted;
    }

    @Tag("audio")
    private class Sound extends Component {

        Sound(String resource) {
            getElement().setProperty("src", resource);
            getElement().setProperty("type",
                    "audio/" + decideType(FilenameUtils.getExtension(resource)));
        }

        void play() {
            getElement().callJsFunction("play");
        }

        void pause() {
            getElement().callJsFunction("pause");
        }

        void load() {
            getElement().callJsFunction("load");
        }

        void mute() {
            this.getElement().setProperty("muted", "true");
        }

        void unmute() {
            this.getElement().removeProperty("muted");
        }

        void setLoop() {
            this.getElement().setProperty("loop", "true");
        }

        void setAutoplay() {
            this.getElement().setProperty("autoplay", "true");
        }

        private String decideType(String extension) {
            switch (extension.toLowerCase()) {
                case "mp3":
                    return "mpeg";
                case "ogg":
                case "wav":
                    return extension;
                default:
                    throw new IllegalArgumentException();
            }
        }
    }
}