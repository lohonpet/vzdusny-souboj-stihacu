package cz.lohonpet.vss.backend.components;

import cz.lohonpet.vss.ui.components.GameBoxUI;

import java.util.List;

public abstract class GameBox {
    private final Coordinates coordinates;
    private Jet currentJet = null;

    GameBox(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    public abstract void setBasicUI(GameBoxUI gameBoxUI);

    Integer getX() {
        return coordinates.getX();
    }

    Integer getY() {
        return coordinates.getY();
    }

    void setCurrentJet(Jet currentJet) {
        this.currentJet = currentJet;
    }

    public Jet getCurrentJet() {
        return currentJet;
    }

    public boolean isJetAvailable() {
        return currentJet != null;
    }

    public boolean isEnemyJetAvailable(PlayerName playerName) {
        return currentJet != null && currentJet.getLeader() != playerName;
    }

    /**
     * Counts possible coordinates reachable with current roll
     *
     * @param roll decides how far is possible to move
     * @return list of possible coordinates to move
     */
    public abstract List<Coordinates> countPossibleMoves(Integer roll);

    public abstract boolean isPlayable();

    public abstract Area getArea();
}
