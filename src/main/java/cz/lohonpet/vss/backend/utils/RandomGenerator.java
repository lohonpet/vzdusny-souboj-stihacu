package cz.lohonpet.vss.backend.utils;


import cz.lohonpet.vss.backend.components.Coordinates;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static cz.lohonpet.vss.utils.VSSConstants.JETS_START_COLUMNS_RANGE;
import static cz.lohonpet.vss.utils.VSSConstants.BOARD_HEIGHT;
import static cz.lohonpet.vss.utils.VSSConstants.BOARD_WIDTH;

public class RandomGenerator {
    private static final Random rand = new Random();

    public static Integer rollDieD6() {
        return rand.nextInt(6) + 1;
    }

    public static List<Integer> rollNTimesDiceD6(Integer ntimes) {
        List<Integer> rolls = new ArrayList<>();
        for (int i = 0; i < ntimes; i++) {
            rolls.add(rollDieD6());
        }
        return rolls;
    }

    public static boolean tossCoin() {
        return rand.nextInt(2) == 1;
    }

    public static Coordinates randomStartJetCoordinate(Integer shift) {
        if (shift > BOARD_WIDTH - JETS_START_COLUMNS_RANGE) {
            throw new IllegalArgumentException("[randomStartJetCoordinate] - wrong argument");
        }
        Integer startJetX = rand.nextInt(JETS_START_COLUMNS_RANGE) + 1 + shift;
        Integer startJetY = rand.nextInt(BOARD_HEIGHT - JETS_START_COLUMNS_RANGE) + JETS_START_COLUMNS_RANGE + 1;
        return new Coordinates(startJetX, startJetY);
    }
}
