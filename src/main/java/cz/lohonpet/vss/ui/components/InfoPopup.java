package cz.lohonpet.vss.ui.components;

import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static cz.lohonpet.vss.utils.VSSConstants.PROTOTYPE;

@Component
@Scope(PROTOTYPE)
public class InfoPopup extends Dialog implements HasComponents {
    private final Button confirmButton = new Button();
    private final H2 header = new H2();
    private final Label content = new Label();
    private final VerticalLayout verticalLayout = new VerticalLayout();

    @Autowired
    public InfoPopup() {
        getElement().setAttribute("theme", "info-popup");
        confirmButton.addClassName("infopopupButton");
        header.addClassName("infopopupHeader");
        content.addClassName("infopopupLabel");
        verticalLayout.addClassName("infopopup");
        verticalLayout.add(header, content, confirmButton);
        add(verticalLayout);
    }

    public Button getConfirmButton() {
        return confirmButton;
    }

    public Label getContent() {
        return content;
    }

    public void addButton(Button button) {
        button.addClassName("infopopupButton");
        verticalLayout.add(button);
    }

    public void setConfirmButtonText(String confirmButtonText) {
        this.confirmButton.setText(confirmButtonText);
    }

    public void setLabelText(String text) {
        this.content.setText(text);
    }

    public void setHeaderText(String text) {
        this.header.setText(text);
    }

    public void setButtonToMainMenu() {
        setConfirmButtonText("Hlavní menu");
        confirmButton.addClickListener(e ->
        {
            close();
            confirmButton.getUI().ifPresent(
                    ui -> ui.navigate(""));
        });
    }

    public void setContentHtml(String html) {
        content.getElement().setProperty("innerHTML", html);
    }
}
