package cz.lohonpet.vss.ui.components;

import cz.lohonpet.vss.backend.presenters.GamePresenterInterface;

import java.util.List;


public class PlayerPanelPc extends PlayerPanel {
    public PlayerPanelPc(GamePresenterInterface gamePresenter) {
        super(gamePresenter);
    }

    @Override
    protected void enableTallyho() {
    }

    @Override
    public void rollDicesUI(List<Integer> rolls) {
        super.rollDicesUI(rolls);
        for (DieUI die : dice) {
            die.removeClickRegistration();
            die.removeHover();
        }
    }

    @Override
    public void disableDieByNumber(Integer roll) {
        for (DieUI die : dice) {
            if (die.isActive() && die.getRoll().equals(roll)) {
                die.deactivate();
                break;
            }
        }
    }

}
