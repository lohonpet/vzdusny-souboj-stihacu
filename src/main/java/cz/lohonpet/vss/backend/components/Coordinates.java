package cz.lohonpet.vss.backend.components;

import java.util.Objects;

import static cz.lohonpet.vss.utils.VSSConstants.*;
import static java.lang.Math.abs;

/**
 * Represents coordinates on the board
 */
public class Coordinates {
    private final Integer x;
    private final Integer y;

    /**
     * @param x coordinate on x axis
     * @param y coordinate on y axis
     */
    public Coordinates(Integer x, Integer y){
        this.x = x;
        this.y = y;
    }

    /**
     * @return x coordinate
     */
    public Integer getX() {
        return x;
    }

    /**
     * @return y coordinate
     */
    public Integer getY() {
        return y;
    }

    /**
     * @param x coordinate for check
     * @param y coordinate for check
     * @return true if coordinates are out of board, false otherwise
     */
    static boolean isOutOfBounds(Integer x, Integer y) {
        return x < BOARD_START_WIDTH || y < BOARD_START_HEIGHT ||
                x > BOARD_WIDTH || y > BOARD_HEIGHT;
    }

    /**
     * Generated method for comparing
     *
     * @param o compared object
     * @return true if objects equals, false otherwise
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Coordinates)) return false;
        Coordinates that = (Coordinates) o;
        return Objects.equals(getX(), that.getX()) &&
                Objects.equals(getY(), that.getY());
    }

    /**
     * Generated method for counting hashcode of instance
     * @return integer with hash computed from x and y coordinate
     */
    @Override
    public int hashCode() {
        return Objects.hash(getX(), getY());
    }

    @Override
    public String toString() {
        return "[" + x + " | " + y + "]";
    }

    public int distanceFrom(Coordinates enemyCoord) {
        return abs(x - enemyCoord.x) + abs(y - enemyCoord.y);
    }
}
