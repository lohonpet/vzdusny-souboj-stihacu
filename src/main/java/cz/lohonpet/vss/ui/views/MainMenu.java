package cz.lohonpet.vss.ui.views;

import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;
import cz.lohonpet.vss.ui.components.GameBoxUI;
import cz.lohonpet.vss.ui.components.MainMenuItem;
import cz.lohonpet.vss.ui.components.MainMenuItemWithPopup;
import cz.lohonpet.vss.ui.utils.SoundLibrary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static cz.lohonpet.vss.utils.VSSConstants.*;

@Route(value = URL_MENU, layout = MainView.class)
@RouteAlias(value = "", layout = MainView.class)
@PageTitle(value = TITLE_MENU)
@CssImport("./styles/menuStyles.css")
@Component
@Scope(PROTOTYPE)
public class MainMenu extends VerticalLayout implements HasComponents {
    private final SoundLibrary soundLibrary;
    private final MainMenuItemWithPopup rules = new MainMenuItemWithPopup(MENU_RULES);
    private final MainMenuItemWithPopup credits = new MainMenuItemWithPopup(MENU_CREDITS);
    private final GameBoxUI muteButton = new GameBoxUI();

    @Autowired
    public MainMenu(SoundLibrary soundLibrary) {
        this.soundLibrary = soundLibrary;
        MainMenuItem gameVsPC = new MainMenuItem(MENU_GAME_VS_PC, URL_GAME_VS_PC);
        MainMenuItem gameVsPlayer = new MainMenuItem(MENU_GAME_VS_PLAYER, URL_GAME_VS_PLAYER);
        HorizontalLayout menuItemsLeft = new HorizontalLayout(gameVsPC, gameVsPlayer);
        menuItemsLeft.addClassName("menuItems");
        setRules();
        setCredits();
        HorizontalLayout menuItemsRight = new HorizontalLayout(rules, credits);
        menuItemsRight.addClassName("menuItems");

        VerticalLayout menuItems = new VerticalLayout(menuItemsLeft, menuItemsRight);
        menuItems.addClassName("menuItemsHorizontal");
        setMuteButton();
        this.soundLibrary.init();
        add(menuItems, this.soundLibrary, this.muteButton);
        this.soundLibrary.setLoopandAutoplay(THEME_SOUND);
    }

    private void setMuteButton() {
        muteButton.addClassNames("muteBoxMenu", "settingsBox");
        if (soundLibrary.isMuted()) {
            muteButton.addClassName("muted");
        } else {
            muteButton.addClassName("unmuted");
        }

        muteButton.addClickListener(e -> {
            if (muteButton.hasClassName("muted")) {
                muteButton.removeClassName("muted");
                muteButton.addClassName("unmuted");
                soundLibrary.unmuteAll();
            } else {
                muteButton.removeClassName("unmuted");
                muteButton.addClassName("muted");
                soundLibrary.muteAll();
            }
        });
    }

    private void setCredits() {
        credits.addInfoPopupContentClass("credits");
        credits.addClickListener(e -> credits.openPopup());
        credits.setHeaderText(MENU_CREDITS);
        credits.addH3WithText("Pravidla");
        credits.addParagraphWithText(PETR_LOHONKA_CREDITS);
        credits.addParagraphWithText(NEJTY_CREDITS);
        credits.addH3WithText("Hlavní grafika");
        credits.addParagraphWithText(NEJTY_CREDITS);
        credits.addH3WithText("Virtuální zpracování");
        credits.addParagraphWithText(PETR_LOHONKA_CREDITS);
        credits.addH3WithText("Drobná grafika pro VZ");
        credits.addParagraphWithText(PETR_LOHONKA_CREDITS);
        credits.addH3WithText("Zvuky");
        credits.addParagraphWithText("dermotte - dice_06.wav " +
                "(https://freesound.org/people/dermotte/sounds/220744/)");
        credits.addParagraphWithText("SoundFX.studio - Spitfire Hispano MK2 Cannon Firing " +
                "(https://freesound.org/people/SoundFX.studio/sounds/464564/)");
        credits.addParagraphWithText("Taira Komori - explosion02.mp3 " +
                "(https://freesound.org/people/Taira%20Komori/sounds/215599/)");
        credits.addParagraphWithText("Lynx_5969 - JET_BYPASS_02.wav " +
                "(https://freesound.org/people/MATTIX/sounds/441500/)");

        credits.addParagraphWithText("dermotte - Coin Drop 1.wav " +
                "(https://freesound.org/people/Lynx_5969/sounds/422671/)");

        credits.addParagraphWithText("vitovsky1 - Fanfare with snare.wav " +
                "(https://freesound.org/people/vitovsky1/sounds/400164/)");
        credits.setButtonToMainMenu();
    }

    private void setRules() {
        rules.addClickListener(e -> rules.openPopup());
        rules.setLabelHtml(RULES);
        rules.setButtonToMainMenu();
    }
}
