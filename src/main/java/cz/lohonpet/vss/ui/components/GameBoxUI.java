package cz.lohonpet.vss.ui.components;

import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.shared.Registration;

public class GameBoxUI extends Div {
    private Registration possibleRegistrations = null;
    private Registration hoverRegistrations = null;

    public GameBoxUI() {
        addClassName("gameBox");
    }

    public void setHoverRegistrations(Registration hoverRegistrations) {
        this.hoverRegistrations = hoverRegistrations;
    }

    public void setPossibleRegistrations(Registration possibleRegistrations) {
        this.possibleRegistrations = possibleRegistrations;
    }

    public void removePossibleRegistration() {
        if (possibleRegistrations != null) {
            possibleRegistrations.remove();
            possibleRegistrations = null;
        }
    }


    private void removeHoverRegistration() {
        if (hoverRegistrations != null) {
            hoverRegistrations.remove();
            hoverRegistrations = null;
        }
    }

    public void removeAllRegistrations() {
        removeHoverRegistration();
        removePossibleRegistration();
    }
    public void addPlayable() {
        this.addClassName("playable");
    }

    public void addGreyBackground() {
        this.addClassName("greyBackground");
    }

    public void addBlueBackground() {
        this.addClassName("blueBackground");
    }

    public void addLeftArrow() {
        this.addClassNames("arrow", "leftArrow");
    }

    public void addRightUpArrow() {
        this.addClassNames("arrow", "rightUpArrow");
    }

    public void addLeftUpArrow() {
        this.addClassNames("arrow", "leftUpArrow");
    }

    public void addUpArrow() {
        this.addClassNames("arrow", "upArrow");
    }

    public void addRightArrow() {
        this.addClassNames("arrow", "rightArrow");
    }

    public void setBackgroundNumber(Integer pictureNumber) {
        this.addClassNames("bkgNum", "bkgNum" + pictureNumber.toString());
    }

    public void deactivateJetUI(String side) {
        this.removeClassName(side + "Jet");
        this.addClassName(side + "JetInactive");
    }

    public void activateJetUI(String side) {
        this.removeClassName(side + "JetInactive");
        this.addClassName(side + "Jet");
    }
}
