package cz.lohonpet.vss.ui.utils;

import com.vaadin.flow.server.VaadinServlet;

import javax.servlet.ServletContext;
import java.net.URL;


class ServletUtils {

    public static String getBaseDirectory(VaadinServlet servlet) {
        return getResourcePath(servlet.getServletContext(), "/");
    }

    private static String getResourcePath(ServletContext servletContext,
                                          String path) {
        String resultPath = servletContext.getRealPath(path);
        if (resultPath != null) {
            return resultPath;
        } else {
            try {
                final URL url = servletContext.getResource(path);
                resultPath = url.getFile();
            } catch (final Exception e) {
                e.printStackTrace();
            }
        }
        return resultPath;
    }
}