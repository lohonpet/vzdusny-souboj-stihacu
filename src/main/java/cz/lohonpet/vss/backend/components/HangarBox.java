package cz.lohonpet.vss.backend.components;

import java.util.ArrayList;
import java.util.List;

import static cz.lohonpet.vss.utils.VSSConstants.*;

public class HangarBox extends TakeOffBox {
    HangarBox(Coordinates coordinates) {
        super(coordinates);
    }

    @Override
    public List<Coordinates> countPossibleMoves(Integer roll) {
        Integer newX, newY = BOARD_START_HEIGHT;
        List<Coordinates> possibleCoords = new ArrayList<>();
        if (getX() < BOARD_WIDTH / 2) {
            newX = LEFT_PLAYER_START_WIDTH - roll + BOARD_START_WIDTH;
        } else {
            newX = RIGHt_PLAYER_START_WIDTH + roll - BOARD_START_WIDTH;
        }

        assert Coordinates.isOutOfBounds(newX, newY) : "[CountPossibleMoves HangarBox] Counted Coordinates out of bounds";
        possibleCoords.add(new Coordinates(newX, newY));
        return possibleCoords;
    }

    @Override
    public boolean isPlayable() {
        return false;
    }
}
