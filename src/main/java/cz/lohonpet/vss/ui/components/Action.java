package cz.lohonpet.vss.ui.components;

import cz.lohonpet.vss.backend.components.Coordinates;
import cz.lohonpet.vss.backend.presenters.GamePresenterInterface;

public abstract class Action {
    private final Coordinates targetCoordinates;

    Action(Coordinates targetCoordinates) {
        this.targetCoordinates = targetCoordinates;
    }

    Coordinates getTargetCoordinates() {
        return targetCoordinates;
    }

    public abstract void highlightBox(GameBoxUI gameBoxUI);

    public abstract void makeAction(GamePresenterInterface gamePresenter, Coordinates originalCoord);

    public abstract boolean isAttackStillPossible(GamePresenterInterface gamePresenter, Coordinates originalCoords);

    public abstract void makeSubstituteAction(GamePresenterInterface gamePresenter, Coordinates originalCoords);
}
