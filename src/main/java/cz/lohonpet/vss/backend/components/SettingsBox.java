package cz.lohonpet.vss.backend.components;

import cz.lohonpet.vss.ui.components.GameBoxUI;

import java.util.List;

public class SettingsBox extends GameBox {

    public SettingsBox(Coordinates coordinates) {
        super(coordinates);
    }

    @Override
    public void setBasicUI(GameBoxUI gameBoxUI) {
        gameBoxUI.addClassName("settingsBox");
    }

    @Override
    public List<Coordinates> countPossibleMoves(Integer roll) {
        return null;
    }

    @Override
    public boolean isPlayable() {
        return false;
    }

    @Override
    public Area getArea() {
        return null;
    }
}
