package cz.lohonpet.vss.backend.presenters;

import com.vaadin.flow.component.UI;
import cz.lohonpet.vss.backend.components.*;
import cz.lohonpet.vss.backend.utils.RandomGenerator;
import cz.lohonpet.vss.ui.components.*;
import cz.lohonpet.vss.ui.views.GameViewInterface;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static cz.lohonpet.vss.utils.VSSConstants.DICE_COUNT;

@Service
public class GamePresenter implements GamePresenterInterface {
    private VssGame game;
    private GameViewInterface gameView;
    private boolean endOfGame = false;

    private Player getPlayerOnTurn() {
        return game.getPlayerOnTurn();
    }

    private void startAutoTurn() {
        Player playerOnTurn = getPlayerOnTurn();
        RollDiceThread rollDiceThread = new RollDiceThread(UI.getCurrent(), gameView, getPlayerOnTurn().rollDices());
        rollDiceThread.start();
        List<PCAction> pcActions = playerOnTurn.computeMoves(game);
        gameView.autoMove(pcActions);
    }

    @Override
    public void rollDices() {
        gameView.rollDicesUI(getPlayerOnTurn().rollDices());
    }

    @Override
    public PlayerName getPlayerOnTurnName() {
        return game.getPlayerOnTurnName();
    }

    @Override
    public void setGameView(GameViewInterface gameView) {
        this.gameView = gameView;
    }

    @Override
    public Map<Coordinates, GameBoxUI> fillBoard() {
        Map<Coordinates, GameBoxUI> boxesUI = new HashMap<>();
        for (Map.Entry<Coordinates, GameBox> box : game.getBoxes().entrySet()) {
            Coordinates currentCoords = box.getKey();
            GameBoxUI gameBoxUI = new GameBoxUI();
            box.getValue().setBasicUI(gameBoxUI);
            boxesUI.put(currentCoords, gameBoxUI);
        }
        return boxesUI;
    }

    @Override
    public List<List<Coordinates>> showJetsOnStart() {
        List<Coordinates> leftArmyCoords = new ArrayList<>();
        List<Coordinates> rightArmyCoords = new ArrayList<>();
        for (Map.Entry<Coordinates, GameBox> box : game.getBoxes().entrySet()) {
            if (box.getValue().isJetAvailable()) {
                if (box.getValue().getCurrentJet().getLeader()
                        == PlayerName.PLAYER_LEFT) {
                    leftArmyCoords.add(box.getKey());
                } else {
                    rightArmyCoords.add(box.getKey());
                }
            }
        }
        List<List<Coordinates>> completeList = new ArrayList<>();
        completeList.add(leftArmyCoords);
        completeList.add(rightArmyCoords);
        return completeList;
    }

    @Override
    public void deactiveJets() {
        game.getPlayerNotOnTurn().deactiveJets();
    }

    @Override
    public void setJetsAfterClickDie(Integer roll) {
        List<Coordinates> jetCoords = game.getPlayerJetsPlayableCoordinates(getPlayerOnTurn());
        if (jetCoords.isEmpty()) {
            switchPlayer();
        } else {
            gameView.setJetsAfterClickDie(jetCoords, roll);
            trySwitchPlayer();
        }
    }

    /**
     * return list of possible actions for one jet and one roll
     *
     * @param coord coordination of single jet
     * @param roll  number of fields jet can move
     * @return list of possible actions
     */
    @Override
    public List<Action> countPossibleMoves(Coordinates coord, Integer roll) {
        return game.createActions(game.countPossibleMoves(coord, roll));
    }

    @Override
    public void moveJetWithSoundAndSwitch(Coordinates originalCoord, Coordinates targetCoordinates) {
        gameView.playMove();
        moveJetWithTrySwitchPlayers(originalCoord, targetCoordinates);
    }

    private void moveJet(Coordinates originalCoord, Coordinates newCoord) {
        gameView.moveJetUI(originalCoord, newCoord, game.getBoxes().get(originalCoord).getCurrentJet().getLeader());
        game.moveJet(originalCoord, newCoord);
    }

    private void moveJetWithTrySwitchPlayers(Coordinates originalCoord, Coordinates targetCoordinates) {
        moveJet(originalCoord, targetCoordinates);
        trySwitchPlayer();
    }

    @Override
    public void shootEnemies(Coordinates originalCoord, Coordinates targetCoord, List<Coordinates> enemies) {
        moveJet(originalCoord, targetCoord);
        boolean tossCoin = false;
        Coordinates shootedEnemy = null;
        for (Coordinates oneEnemy : enemies) {
            tossCoin = RandomGenerator.tossCoin();
            if (tossCoin) {
                shootedEnemy = oneEnemy;
                break;
            }
        }
        gameView.showTossCoin(tossCoin);
        ShootThread shootThread = new ShootThread(UI.getCurrent(), gameView, this, targetCoord, shootedEnemy);
        shootThread.start();
    }

    @Override
    public void killEnemyWithSound(Coordinates originalCoord, Coordinates targetCoord) {
        killEnemy(originalCoord, targetCoord);
        gameView.playKill();
    }

    public void killEnemy(Coordinates originalCoord, Coordinates targetCoord) {
        moveJet(originalCoord, targetCoord);
        endOfGame = game.isEndOfGame();
        if (endOfGame) {
            endGame();
        } else {
            trySwitchPlayer();
        }
    }

    private void endGame() {
        gameView.endGame();
    }

    @Override
    public boolean isEndOfGame() {
        return endOfGame;
    }

    @Override
    public void cleanEndGame() {
        endOfGame = false;
    }

    @Override
    public void trySwitchPlayer() {
        if (gameView.areAllDiceUsed(getPlayerOnTurnName())) {
            switchPlayer();
        } else if (isPlayerStuck(game.getPlayerJetsPlayableCoordinates(getPlayerOnTurn()))) {
            if (!(game.getJetCoordsOfPlayer(game.getPlayerOnTurnName()).size()
                    < DICE_COUNT)) {
                StuckedPopupThread stuckThread = new StuckedPopupThread(UI.getCurrent(), gameView);
                stuckThread.start();
            }
            switchPlayer();
        }
    }

    private void switchPlayer() {
        PlayerName originalPlayerOnTurn = getPlayerOnTurnName();
        PlayerName originalPlayerNotOnTurn = getPlayerNotOnTurnName();
        gameView.deactivatePlayerUI(originalPlayerOnTurn, game.getJetCoordsOfPlayer(originalPlayerOnTurn));
        gameView.activatePlayerUI(originalPlayerNotOnTurn, game.getJetCoordsOfPlayer(originalPlayerNotOnTurn));
        game.switchPlayer();
        if (getPlayerOnTurn().needsAutoTurn()) {
            startAutoTurn();
        }
    }

    private PlayerName getPlayerNotOnTurnName() {
        return game.getPlayerNotOnTurnName();
    }

    private boolean isPlayerStuck(List<Coordinates> jetCoords) {
        final List<DieUI> activeDice = gameView.getActiveDice();
        if (jetCoords.isEmpty() && activeDice.isEmpty()) {
            return false;
        }
        for (DieUI die : activeDice) {
            for (Coordinates oneCoord : jetCoords) {
                List<Coordinates> tempCoord = game.countPossibleMoves(oneCoord, die.getRoll());
                if (!tempCoord.isEmpty()) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void playDice() {
        gameView.playDice();
    }

    @Override
    public void setGame(VssGame vssGame) {
        this.game = vssGame;
    }

    @Override
    public void muteSounds() {
        gameView.muteSounds();
    }

    @Override
    public void unmuteSounds() {
        gameView.unmuteSounds();
    }

    @Override
    public void openSettingsPopup() {
        gameView.openSettingsPopup();
    }

    @Override
    public boolean isEnemyJetOnCoord(Coordinates targetCoordinates, Coordinates originalCoords) {
        return game.isEnemyJetOnCoord(targetCoordinates, originalCoords);
    }
}
