package cz.lohonpet.vss.ui.components;

import com.vaadin.flow.component.UI;
import cz.lohonpet.vss.ui.views.GameViewInterface;

public class StuckedPopupThread extends Thread {
    private final UI ui;
    private final GameViewInterface gameView;

    public StuckedPopupThread(UI ui, GameViewInterface view) {
        this.ui = ui;
        this.gameView = view;
    }

    @Override
    public void run() {
        try {
            ui.access(gameView::showPlayerStuckPopup);
            Thread.sleep(2500);
            ui.access(gameView::closeInfoPopup);

        } catch (
                InterruptedException e) {
            e.printStackTrace();

        }

    }
}
