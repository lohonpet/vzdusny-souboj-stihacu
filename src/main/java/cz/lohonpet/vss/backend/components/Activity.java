package cz.lohonpet.vss.backend.components;

/**
 * Enum for displaying component status
 */
public enum Activity {
    ACTIVE, INACTIVE
}
