package cz.lohonpet.vss.utils;

public class VSSConstants {
    private static final String APP_NAME = "Vzdušný souboj stíhačů";
    public static final String PWA_NAME = "Vzdusny souboj stihacu";

    public static final String URL_MENU = "menu";
    public static final String URL_GAME = "game";
    public static final String URL_PC = "pc";
    private static final String URL_PLAYER = "player";
    public static final String URL_GAME_VS_PC = URL_GAME + "/" + URL_PC;
    public static final String URL_GAME_VS_PLAYER = URL_GAME + "/" + URL_PLAYER;

    private static final String TITLE_SEPARATOR = " | ";
    private static final String TITLE_END = TITLE_SEPARATOR + APP_NAME;
    public static final String TITLE_MENU = "Hlavní menu" + TITLE_END;
    public static final String TITLE_GAME = "Zničte nepřítele a získejte vítězství!" + TITLE_END;

    public static final String MENU_GAME_VS_PC = "Hra proti PC";
    public static final String MENU_GAME_VS_PLAYER = "1 vs. 1";
    public static final String MENU_RULES = "Pravidla";
    public static final String MENU_CREDITS = "Autorství";

    public static final String PETR_LOHONKA_CREDITS = "Petr Lohonka";
    public static final String NEJTY_CREDITS = "Nejty";

    public static final Integer BOARD_START_WIDTH = 1;
    public static final Integer BOARD_START_HEIGHT = 1;
    public static final Integer BOARD_WIDTH = 17;
    public static final Integer BOARD_HEIGHT = 13;

    public static final Integer LEFT_PLAYER_START_WIDTH = 7;
    public static final Integer RIGHt_PLAYER_START_WIDTH = 11;


    public static final String VELITEL_ONE = "Velitel 1";
    public static final String VELITEL_TWO = "Velitel 2";
    public static final String PC_NAME = "Dron 2";

    public static final String HIGHLIGHT = "highlight";


    public static final String SOUNDS_DIRECTORY = "/sounds/";
    public static final String WEBAPP_DIRECTORY = "webapp";
    public static final String DICE_SOUND = "dice";
    public static final String MOVE_SOUND = "move";
    public static final String SHOOT_SOUND = "shoot";
    public static final String KILL_SOUND = "kill";
    public static final String COIN_SOUND = "coin";
    public static final String THEME_SOUND = "theme";

    public static final Integer JETS_QUANTITY = 6;
    public static final Integer JETS_IN_HANGAR_QUANTITY = 3;
    public static final Integer JETS_START_COLUMNS_RANGE = 5;
    public static final Integer DICE_COUNT = 3;
    public static final Integer SHOOTING_AREA = 1;
    public static final String PROTOTYPE = "prototype";

    public static final Integer KILL_PRIORITY = 100;
    public static final Integer AIM_PRIORITY = 80;
    public static final Integer START_DEFEND_PRIORITY = -25;
    public static final Integer MIDDLE_DEFEND_PRIORITY = -50;
    public static final Integer END_DEFEND_PRIORITY = -40;
    public static final Integer AIM_DEFEND_PRIORITY = -20;
    public static final Integer AGGRESSION_FACTOR_PRIORITY = 20;
    public static final Integer MOVE_PRIORITY = 8;
    public static final Integer TAKEOFF_BONUS = 50;
    public static final Integer TAKEOFF_PRIORITY = 10;
    public static final Integer LAST_MOVE_PRIORITY = -500;


    public static final String BACK_BUTTON_TEXT = "Zpátky";
    public static final String MUTE_BUTTON_TEXT = "Ztlumit zvuk";
    public static final String UNMUTE_BUTTON_TEXT = "Zapnout zvuk";
    public static final String SETTINGS_BUTTON_TEXT = "Nastavení";

    public static final String RULES = "<h1>Vzdu&scaron;n&yacute; souboj &ndash; pravidla</h1>\n" +
            "<h2>Z&aacute;kladn&iacute; pravidla</h2>\n" +
            "<ul>\n" +
            "<li>Hraj&iacute; <strong>2 hr&aacute;či</strong> proti sobě</li>\n" +
            "<li>Každ&yacute; hr&aacute;č m&aacute; na zač&aacute;tku hry celkem <strong>6 st&iacute;haček </strong>rozli&scaron;en&yacute;ch barvou<strong> (3 ve vzduchu a 3 v&nbsp;hang&aacute;ru)</strong></li>\n" +
            "<li>Jsou 2 druhy pol&iacute;ček:\n" +
            "<ol>\n" +
            "<li><em>vzletov&aacute;</em> - pol&iacute;čka označen&aacute; č&iacute;sly 1, 2, 3 a pol&iacute;čka s&nbsp;b&iacute;l&yacute;m troj&uacute;heln&iacute;kem &ndash; &scaron;ipkou</li>\n" +
            "<li><em>bojov&aacute;</em> &ndash; s&iacute;ť pr&aacute;zdn&yacute;ch pol&iacute;ček na &bdquo;obloze&ldquo;</li>\n" +
            "</ol>\n" +
            "</li>\n" +
            "<li>Hr&aacute;či se <strong>stř&iacute;daj&iacute; v&nbsp;taz&iacute;ch</strong></li>\n" +
            "<li>Hr&aacute;č hod&iacute; v&nbsp;každ&eacute;m tahu 3 kostkami a <strong>každ&yacute; jeden hod přiřad&iacute; k&nbsp;jedn&eacute; sv&eacute; st&iacute;hačce</strong>, kter&aacute; se o hozen&yacute; počet pol&iacute;ček pohybuje (d&aacute;le viz sekce <em>Pohyb st&iacute;haček</em>).</li>\n" +
            "<li>Po vzletov&yacute;ch pol&iacute;čk&aacute;ch se pohybuje jen ve směru z&nbsp;hang&aacute;ru do nebe (d&aacute;le viz sekce <em>Pohyb po vzletov&yacute;ch pol&iacute;čk&aacute;ch</em>).</li>\n" +
            "<li>Pohyb st&iacute;hačky po bojov&yacute;ch pol&iacute;čk&aacute;ch je možn&yacute; <strong>vertik&aacute;lně nebo horizont&aacute;lně</strong>.</li>\n" +
            "<li>Pokud st&iacute;hačka skonč&iacute; svůj tah na pol&iacute;čku, kde je st&iacute;hačka protivn&iacute;ka, tak ji sestřel&iacute;.</li>\n" +
            "<li>Pokud st&iacute;hačka skonč&iacute; svůj tah na pol&iacute;čku <strong>soused&iacute;c&iacute;m hranou</strong> s pol&iacute;čkem, kde je st&iacute;hačka protivn&iacute;ka, tak ji dostane do zaměřovače, za&uacute;toč&iacute; a m&aacute; 50% &scaron;anci na jej&iacute; sestřelen&iacute; (d&aacute;le viz sekce Sestřelen&iacute;). .</li>\n" +
            "<li>Vyhr&aacute;v&aacute; ten hr&aacute;č, jenž sestřel&iacute; protivn&iacute;kovi v&scaron;echna letadla na bojov&yacute;ch pol&iacute;čk&aacute;ch.</li>\n" +
            "</ul>\n" +
            "<h2>Detailn&iacute; pravidla</h2>\n" +
            "<h3>Zahájení hry</h3>\n" +
            "<ul>\n" +
            "<li>Každ&yacute; hr&aacute;č n&aacute;hodně rozestav&iacute; 3 st&iacute;hačky v&nbsp;1 &ndash; 5 (resp. 12-17) sloupci bojov&yacute;ch pol&iacute;ček &ndash; ty hl&iacute;dkuj&iacute; ve vzduchu.</li>\n" +
            "<li>Zbyl&eacute; 3 st&iacute;hačky zač&iacute;naj&iacute; v&nbsp;hang&aacute;ru na pol&iacute;čk&aacute;ch 1, 2, 3 &ndash; ty jsou připraveny a čekaj&iacute; na start.</li>\n" +
            "<li>N&aacute;hodně je vybr&aacute;n zač&iacute;naj&iacute;c&iacute; hr&aacute;č.</li>\n" +
            "</ul>\n" +
            "<h3>Pravidla pohybu</h3>\n" +
            "<p>Hr&aacute;č stř&iacute;davě h&aacute;zej&iacute; vždy 3 kostkami, kter&eacute; představuj&iacute; pohyb st&iacute;haček po obloze/při vzletu, vždy plat&iacute; pravidlo <strong>přesně</strong> <strong>jeden hod pro jedno letadlo</strong>:</p>\n" +
            "<h4>Pohyb po bojov&yacute;ch pol&iacute;ch</h4>\n" +
            "<p>Pohyb je možn&yacute; <strong>vertik&aacute;lně či horizont&aacute;lně</strong>. Je zak&aacute;z&aacute;n pohyb po diagon&aacute;le. <strong>Ostatn&iacute; st&iacute;hačky pohyb neblokuj&iacute;</strong>, tedy je možn&eacute; se pohybovat i směrem, v&nbsp;němž stoj&iacute; vlastn&iacute; či nepř&aacute;telsk&eacute; letadlo a přeskočit ho (v realitě by se prostě vyhnuly v&nbsp;různ&eacute; v&yacute;&scaron;ce).</p>\n" +
            "<p>Nen&iacute; dovoleno m&iacute;t 2 a v&iacute;ce st&iacute;haček na jednom pol&iacute;čku &ndash; pokud je na c&iacute;lov&eacute;m pol&iacute;čku nepř&aacute;telsk&aacute; st&iacute;hačka, je zničena, pokud př&aacute;telsk&aacute; st&iacute;hačka, pohyb nen&iacute; možn&yacute;.</p>\n" +
            "<p>Pokud nen&iacute; možn&eacute; se pohnout nijak (např. jsem v&nbsp;rohu mapy a jedin&aacute; 2 možn&aacute; pol&iacute;čka pro pohyb jsou obsazena vlastn&iacute;mi st&iacute;hačkami), hod propad&aacute; a pokračuje soupeř.</p>\n" +
            "<p>Pokud m&aacute; hr&aacute;č na bojov&yacute;ch pol&iacute;čk&aacute;ch m&eacute;ně než 3 letadla, st&aacute;le h&aacute;že 3 kostkami a po proveden&iacute; pohybu st&iacute;haček na bojov&yacute;ch pol&iacute;čk&aacute;ch může zb&yacute;vaj&iacute;c&iacute; hod (hody) použ&iacute;t pro letadla na vzletov&yacute;ch pol&iacute;čk&aacute;ch.</p>\n" +
            "<p><em>Př. 1: Na bojov&yacute;ch pol&iacute;čk&aacute;ch m&aacute;m 2 letadla, na vzletov&yacute;ch 3 letadla. Hod&iacute;m 5, 3, 4 na kostk&aacute;ch. 2 hody mus&iacute;m použ&iacute;t na obě letadla na bojov&yacute;ch pol&iacute;čk&aacute;ch (např. hody 3 a 4), pot&eacute; si vyberu jedno z&nbsp;letadel na vzletu a to posunu o zb&yacute;vaj&iacute;c&iacute; hod, tedy 5 pol&iacute;ček.</em></p>\n" +
            "<p>Pokud m&aacute; hr&aacute;č na bojov&yacute;ch pol&iacute;čk&aacute;ch m&eacute;ně než 3 letadla a na vzletov&yacute;ch pol&iacute;čk&aacute;ch už ž&aacute;dn&eacute; letadlo nezb&yacute;v&aacute;, může si z&nbsp;hodů vybrat. Nepoužit&eacute; hody kostkou propadaj&iacute;.</p>\n" +
            "<p><em>Př. 2: Na bojov&yacute;ch pol&iacute;čk&aacute;ch m&aacute;m 2 letadla, na vzletov&yacute;ch ž&aacute;dn&eacute;. Hod&iacute;m 5, 3, 4 na kostk&aacute;ch. 2 hody mus&iacute;m použ&iacute;t na obě letadla na bojov&yacute;ch pol&iacute;čk&aacute;ch (např. hody 3 a 4), hod s&nbsp;hodnotou 5 nelze použ&iacute;t nikde, proto propad&aacute; a hraje soupeř.</em></p>\n" +
            "<h4>Pohyb po vzletov&yacute;ch pol&iacute;ch</h4>\n" +
            "<p>Po vzletov&yacute;ch pol&iacute;čk&aacute;ch se pohybuje <strong>jen ve směru z&nbsp;hang&aacute;ru do nebe</strong>, pohyb zač&iacute;n&aacute; vždy na zač&aacute;tku ranveje.</p>\n" +
            "<p>Pohyb po vzletov&yacute;ch pol&iacute;čk&aacute;ch nast&aacute;v&aacute; pouze ve 2 př&iacute;padech:</p>\n" +
            "<ul>\n" +
            "<li>Pokud padne <strong>alespoň na jedn&eacute; kostce 6</strong>, pak <strong>v&scaron;echny 3 hody mus&iacute; b&yacute;t použity pro letadla na vzletov&yacute;ch pol&iacute;čk&aacute;ch</strong>, opět jeden hod pro jedno letadlo. Pokud jsou na vzletov&yacute;ch pol&iacute;čk&aacute;ch m&eacute;ně než 3 letadla, lze zb&yacute;vaj&iacute;c&iacute; hody použ&iacute;t pro letadlo či letadla na bojov&yacute;ch pol&iacute;čk&aacute;ch.</li>\n" +
            "</ul>\n" +
            "<p><em>Př. 3: Na bojov&yacute;ch pol&iacute;čk&aacute;ch m&aacute;m 2 letadla, na vzletov&yacute;ch 3 st&iacute;hačky. Hod&iacute;m 6, 3, 1 na kostk&aacute;ch. V&scaron;echny hody mus&iacute;m použ&iacute;t na letadla na vzletov&yacute;ch pol&iacute;čk&aacute;ch. Letadla na bojov&yacute;ch pol&iacute;čk&aacute;ch se v&nbsp;tomto tahu neh&yacute;baj&iacute;</em>.</p>\n" +
            "<p><em>Př. 4: Na bojov&yacute;ch pol&iacute;čk&aacute;ch m&aacute;m 3 letadla, na vzletov&yacute;ch tak&eacute; 3 st&iacute;hačky. Hod&iacute;m 5, 3, 1 na kostk&aacute;ch. V&scaron;echny hody mus&iacute;m použ&iacute;t na letadla na bojov&yacute;ch pol&iacute;čk&aacute;ch. Letadla na vzletov&yacute;ch pol&iacute;čk&aacute;ch se v&nbsp;tomto tahu neh&yacute;baj&iacute;.</em></p>\n" +
            "<p><em>Př. 5: Na bojov&yacute;ch pol&iacute;čk&aacute;ch m&aacute;m 3 letadla, na vzletov&yacute;ch 2 st&iacute;hačky. Hod&iacute;m 5, 3, 6 na kostk&aacute;ch. 2 hody mus&iacute;m použ&iacute;t na letadla na vzletov&yacute;ch pol&iacute;čk&aacute;ch (např. 5 a 3). Pot&eacute; si vyberu jedno z&nbsp;letadel na bojov&yacute;ch pol&iacute;čk&aacute;ch a to posunu o zb&yacute;vaj&iacute;c&iacute; hod, tedy 6 pol&iacute;ček.</em></p>\n" +
            "<ul>\n" +
            "<li>Pokud m&aacute; hr&aacute;č na bojov&yacute;ch pol&iacute;čk&aacute;ch m&eacute;ně než 3 letadla st&aacute;le h&aacute;že 3 kostkami a po proveden&iacute; pohybu st&iacute;haček na bojov&yacute;ch pol&iacute;čk&aacute;ch <strong>může zb&yacute;vaj&iacute;c&iacute; hod (hody) použ&iacute;t pro letadla na vzletov&yacute;ch pol&iacute;čk&aacute;ch</strong> (viz př&iacute;klad 1 v sekci <em>Pohyb po bojov&yacute;ch pol&iacute;čk&aacute;ch</em>).</li>\n" +
            "</ul>\n" +
            "<h3>C&iacute;l hry</h3>\n" +
            "<p>Vyhr&aacute;v&aacute; ten hr&aacute;č, kter&yacute; <strong>protivn&iacute;kovi sestřel&iacute; v&scaron;echna letadla na bojov&yacute;ch pol&iacute;čk&aacute;ch</strong> (př&iacute;padn&aacute; startuj&iacute;c&iacute; letadla jsou snadnou kořist&iacute;, protože nemaj&iacute; v&yacute;&scaron;ku a rychlost).</p>\n" +
            "<p>Sestřelit nepř&iacute;tele lze 2 způsoby:</p>\n" +
            "<h4>Jasn&yacute; sestřel</h4>\n" +
            "<p>Hr&aacute;č dokonč&iacute; svůj pohyb př&iacute;mo na pol&iacute;čku se soupeřem a t&iacute;m ho sestřel&iacute;.</p>\n" +
            "<h4>Nepřítel v zaměřovači</h4>\n" +
            "<p>Hr&aacute;č dokonč&iacute; svůj pohyb př&iacute;mo na pol&iacute;čku <strong>soused&iacute;c&iacute;m hranou se soupeřem. </strong>To si můžeme představit tak, že vyman&eacute;vroval soupeře a m&aacute; ho v&nbsp;zaměřovači. Pot&eacute; si je&scaron;tě <strong>v&nbsp;tom sam&eacute;m tahu hod&iacute; minc&iacute;</strong> &ndash; panna&nbsp; = soupeř je zničen, orel = soupeř man&eacute;vrem na posledn&iacute; chv&iacute;li unikl z&aacute;sahu.</p>\n" +
            "<h3>FAQ</h3>\n" +
            "<ul>\n" +
            "<li>H&aacute;z&iacute; se znovu po hozen&iacute; &scaron;estky? Neh&aacute;z&iacute;.</li>\n" +
            "<li>Sm&iacute; se po vzl&eacute;tnut&iacute; man&eacute;vrovat zp&aacute;tky na vzletov&aacute; pol&iacute;čka? Nesm&iacute;.</li>\n" +
            "</ul>";

}
