package cz.lohonpet.vss.backend.presenters;

import cz.lohonpet.vss.backend.components.Coordinates;
import cz.lohonpet.vss.backend.components.PlayerName;
import cz.lohonpet.vss.backend.components.VssGame;
import cz.lohonpet.vss.ui.components.Action;
import cz.lohonpet.vss.ui.components.GameBoxUI;
import cz.lohonpet.vss.ui.views.GameViewInterface;

import java.util.List;
import java.util.Map;

public interface GamePresenterInterface {
    void rollDices();
    void setGameView(GameViewInterface gameView);
    Map<Coordinates, GameBoxUI> fillBoard();
    List<List<Coordinates>> showJetsOnStart();
    void deactiveJets();
    void setJetsAfterClickDie(Integer roll);
    List<Action> countPossibleMoves(Coordinates coord, Integer roll);
    void moveJetWithSoundAndSwitch(Coordinates original, Coordinates target);
    void shootEnemies(Coordinates original, Coordinates target, List<Coordinates> enemies);
    void killEnemyWithSound(Coordinates originalCoord, Coordinates targetCoord);
    void killEnemy(Coordinates originalCoord, Coordinates targetCoord);
    void playDice();
    void muteSounds();
    void unmuteSounds();
    void openSettingsPopup();
    boolean isEnemyJetOnCoord(Coordinates target, Coordinates original);
    void setGame(VssGame vssGame);
    PlayerName getPlayerOnTurnName();
    void trySwitchPlayer();

    boolean isEndOfGame();

    void cleanEndGame();
}
