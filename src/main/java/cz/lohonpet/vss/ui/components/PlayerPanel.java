package cz.lohonpet.vss.ui.components;

import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.shared.Registration;
import cz.lohonpet.vss.backend.presenters.GamePresenterInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Scope(value = "prototype")
public class PlayerPanel extends Composite<VerticalLayout> implements HasStyle, HasComponents {
    final List<DieUI> dice;
    private final Div tallyho;
    private final GamePresenterInterface gamePresenter;
    private final H2 velitelName;
    private Registration regTallyho;

    @Autowired
    public PlayerPanel(GamePresenterInterface gamePresenter) {
        this.gamePresenter = gamePresenter;
        addClassName("playerPanel");
        this.velitelName = new H2();
        velitelName.addClassName("velitelLabel");
        dice = new ArrayList<>();
        dice.add(new DieUI("Red"));
        dice.add(new DieUI("Orange"));
        dice.add(new DieUI("Green"));
        tallyho = new Div();
        tallyho.addClassNames("tallyho");
        tallyho.setText("Do boje!");
        enableTallyho();
        add(velitelName);
        for (DieUI die : dice) {
            add(die);
        }
        add(tallyho);
    }

    public void setPlayerPanel(String velitelName, String className) {
        setVelitelName(velitelName);
        addClassName(className);
    }

    private void setVelitelName(String velitelName) {
        this.velitelName.setText(velitelName);
    }

    public void rollDicesUI(List<Integer> rolls) {
        gamePresenter.playDice();
        int i = 0;
        for (DieUI die : dice) {
            die.setRoll(rolls.get(i++));
            die.animate();
            Registration clickDieReg = die.addClickListener(e -> clickDie(die));
            die.setClickRegistration(clickDieReg);
        }
        disableTallyho();
    }

    private void clickDie(DieUI die) {
        for (DieUI oneDie : dice) {
            oneDie.removeClassName("activeDie");
        }
        die.addClassName("activeDie");
        gamePresenter.setJetsAfterClickDie(die.getRoll());
    }

    private void disableTallyho() {
        if (regTallyho != null) {
            regTallyho.remove();
            regTallyho = null;
        }
        tallyho.addClassName("disableTallyho");
    }

    void enableTallyho() {
        regTallyho = tallyho.addClickListener(e -> gamePresenter.rollDices());
        tallyho.removeClassName("disableTallyho");
    }

    public void deactivate() {
        disableTallyho();
        deactivateDice();
        gamePresenter.deactiveJets();
    }

    public void activate() {
        enableTallyho();
        activateDice();
    }

    private void activateDice() {
        for (DieUI die : dice) {
            die.activate();
        }
    }

    public void disableActiveDie() {
        for (DieUI die : dice) {
            if (die.hasClassName("activeDie")) {
                die.deactivate();
            }
        }
    }

    public void disableDieByNumber(Integer roll) {
    }

    private void deactivateDice() {
        for (DieUI die : dice) {
            die.deactivate();
        }
    }

    public boolean areAllDiceUsed() {
        for (DieUI die : dice) {
            if (die.isActive()) {
                return false;
            }
        }
        return true;
    }


    public List<DieUI> getActiveDice() {
        List<DieUI> activeDice = new ArrayList<>();
        for (DieUI die : dice) {
            if (die.isActive()) {
                activeDice.add(die);
            }
        }

        return activeDice;
    }
}
