package cz.lohonpet.vss.ui.components;

import cz.lohonpet.vss.backend.components.Coordinates;
import cz.lohonpet.vss.backend.presenters.GamePresenterInterface;

import java.util.List;

import static cz.lohonpet.vss.utils.VSSConstants.*;

public class Shoot extends Action {
    private final List<Coordinates> enemies;

    public Shoot(Coordinates targetCoordinates, List<Coordinates> enemies) {
        super(targetCoordinates);
        this.enemies = enemies;
    }

    @Override
    public void highlightBox(GameBoxUI gameBoxUI) {
        gameBoxUI.addClassNames(HIGHLIGHT, "crosshair");
    }

    @Override
    public void makeAction(GamePresenterInterface gamePresenter, Coordinates originalCoord) {
        gamePresenter.shootEnemies(originalCoord, getTargetCoordinates(), enemies);
    }

    @Override
    public boolean isAttackStillPossible(GamePresenterInterface gamePresenter, Coordinates originalCoords) {
        enemies.removeIf(oneEnemy -> !gamePresenter.isEnemyJetOnCoord(oneEnemy, originalCoords));
        return enemies.size() > 0;
    }

    @Override
    public void makeSubstituteAction(GamePresenterInterface gamePresenter, Coordinates originalCoords) {
        new Move(getTargetCoordinates()).makeAction(gamePresenter, originalCoords);
    }
}
