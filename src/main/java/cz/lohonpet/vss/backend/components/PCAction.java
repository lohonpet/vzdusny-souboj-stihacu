package cz.lohonpet.vss.backend.components;

import cz.lohonpet.vss.backend.presenters.GamePresenterInterface;
import cz.lohonpet.vss.ui.components.Action;

public class PCAction {
    private final Action action;
    private final Integer roll;
    private final Coordinates originalCoords;

    public PCAction(Action action, Integer roll, Coordinates originalCoords) {
        this.action = action;
        this.roll = roll;
        this.originalCoords = originalCoords;
    }

    public void makeAction(GamePresenterInterface gamePresenter) {
        if (action.isAttackStillPossible(gamePresenter, originalCoords)) {
            action.makeAction(gamePresenter, originalCoords);
        } else {
            action.makeSubstituteAction(gamePresenter, originalCoords);
        }
    }

    public Integer getRoll() {
        return roll;
    }
}
