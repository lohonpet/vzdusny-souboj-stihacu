package cz.lohonpet.vss.ui.components;

import com.vaadin.flow.component.UI;
import cz.lohonpet.vss.backend.components.Coordinates;
import cz.lohonpet.vss.backend.presenters.GamePresenterInterface;
import cz.lohonpet.vss.ui.views.GameViewInterface;

public class ShootThread extends Thread {
    private final UI ui;
    private final GameViewInterface gameView;
    private final GamePresenterInterface gamePresenter;
    private final Coordinates attackerCoord;
    private final Coordinates shootedEnemy;


    public ShootThread(UI ui, GameViewInterface view, GamePresenterInterface gamePresenter,
                       Coordinates attackerCoord, Coordinates shootedEnemy) {
        this.ui = ui;
        this.gameView = view;
        this.gamePresenter = gamePresenter;
        this.attackerCoord = attackerCoord;
        this.shootedEnemy = shootedEnemy;
    }


    @Override
    public void run() {
        try {
            Thread.sleep(2300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ui.access(() -> {
            if (null != shootedEnemy) {
                gameView.cleanTossCoinPopup();
                gameView.playShoot();
                gamePresenter.killEnemy(attackerCoord, shootedEnemy);
            } else {
                gameView.cleanTossCoinPopup();
                gamePresenter.trySwitchPlayer();
            }
        });

    }
}
