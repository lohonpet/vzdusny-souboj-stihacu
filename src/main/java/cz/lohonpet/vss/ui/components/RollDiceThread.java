package cz.lohonpet.vss.ui.components;

import com.vaadin.flow.component.UI;
import cz.lohonpet.vss.ui.views.GameViewInterface;

import java.util.List;

public class RollDiceThread extends Thread {
    private final UI ui;
    private final GameViewInterface gameView;
    private final List<Integer> rolls;

    public RollDiceThread(UI ui, GameViewInterface view, List<Integer> rolls) {
        this.ui = ui;
        this.gameView = view;
        this.rolls = rolls;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(1500);
            if (gameView.isInfoPopupOpen()) {
                Thread.sleep(1800);
            }
            ui.access(() -> gameView.rollDicesUI(rolls));

        } catch (
                InterruptedException e) {
            e.printStackTrace();

        }

    }
}
