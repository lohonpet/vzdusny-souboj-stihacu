package cz.lohonpet.vss.ui.views;

import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.router.RouterLayout;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.server.PWA;

import static cz.lohonpet.vss.utils.VSSConstants.PWA_NAME;

@Push

@PWA(name = PWA_NAME, shortName = "VSS", backgroundColor = "#00aae3", themeColor = "#00aae3", display = "fullscreen")
@CssImport("./styles/mainStyles.css")
@CssImport(value = "./styles/my-overlay-theme.css",
        themeFor = "vaadin-*-overlay")
class MainView extends Composite<VerticalLayout> implements HasComponents, RouterLayout {
    private final Div childWrapper = new Div();

    public MainView() {
        Image title = new Image("images/title.png", "Vzdušný souboj stíhačů");
        title.addClassNames("title", "titleGame");
        childWrapper.addClassName("appBackground");
        childWrapper.add(title);
        add(childWrapper);
    }

    public void showRouterLayoutContent(HasElement content) {
        childWrapper.getElement().appendChild(content.getElement());
    }
}
