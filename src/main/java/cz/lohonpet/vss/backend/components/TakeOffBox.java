package cz.lohonpet.vss.backend.components;

import cz.lohonpet.vss.ui.components.GameBoxUI;

import java.util.ArrayList;
import java.util.List;

import static cz.lohonpet.vss.utils.VSSConstants.*;

public class TakeOffBox extends GameBox {

    public TakeOffBox(Coordinates coordinates) {
        super(coordinates);
    }

    @Override
    public void setBasicUI(GameBoxUI gameBoxUI) {
        gameBoxUI.addPlayable();
        if ((getX() == 1 || getX() == 17) && getY() != 1) {
            gameBoxUI.addBlueBackground();
            gameBoxUI.addUpArrow();
        } else if (getY() == 1) {
            gameBoxUI.addGreyBackground();
            if (getX() >= 2 && getX() <= 7) {
                gameBoxUI.addLeftArrow();
            } else if (getX() >= 11 && getX() <= 16) {
                gameBoxUI.addRightArrow();
            } else if (getX() == 1) {
                gameBoxUI.addLeftUpArrow();
            } else {
                gameBoxUI.addRightUpArrow();
            }
        } else {
            Integer diff = getX() < 9 ? 4 : 10;
            gameBoxUI.setBackgroundNumber(getX() - diff);
        }
    }

    @Override
    public List<Coordinates> countPossibleMoves(Integer roll) {
        Integer newX, newY;
        List<Coordinates> possibleCoords = new ArrayList<>();
        if (getY().equals(BOARD_START_HEIGHT)) {
            if (getX() < BOARD_WIDTH / 2) {
                newX = getX() - roll;
                newY = getY();
                if (Coordinates.isOutOfBounds(newX, newY)) {
                    newY = getY() + (-newX) + BOARD_START_HEIGHT;
                    newX = BOARD_START_WIDTH;
                }
            } else {
                newX = getX() + roll;
                newY = getY();
                if (Coordinates.isOutOfBounds(newX, newY)) {
                    newY = getY() + newX - BOARD_WIDTH;
                    newX = BOARD_WIDTH;
                }
            }
        } else {
            newX = getX();
            newY = getY() + roll;
        }
        assert Coordinates.isOutOfBounds(newX, newY) : "[CountPossibleMoves TakeOff] Counted Coordinates out of bounds";
        possibleCoords.add(new Coordinates(newX, newY));
        return possibleCoords;
    }

    @Override
    public boolean isPlayable() {
        return true;
    }

    @Override
    public Area getArea() {
        return Area.TAKEOFF;
    }

}
