package cz.lohonpet.vss.backend.components;

public class Jet {
    private PlayerName leader;
    private Activity activity = Activity.ACTIVE;

    Jet() {
    }

    public PlayerName getLeader() {
        return leader;
    }

    void setLeader(PlayerName leader) {
        this.leader = leader;
    }

    private void setActivity(Activity activity) {
        this.activity = activity;
    }

    public boolean isActive() {
        return activity == Activity.ACTIVE;
    }

    public void activate() {
        setActivity(Activity.ACTIVE);
    }

    void deactivate() {
        setActivity(Activity.INACTIVE);
    }
}
